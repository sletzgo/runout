# Runout

An awesome game coded in Java by Bas & Lawrence


## Name idea list

- Runout


## Story

Player spawns.

**Objectives**

Hunt down assl


## Player

The player is able to move around in the world. Up down left and right.
The player is able to pick up different items.
All players have the same view range
Each player can have different abilities


## Terrain

### Technical notes

**Types of coordinates:**
- Global tile coordinates (long).
- Local tile coordinates (int).
- Chunk coordinates (int).


## Entity list

- Player
- Item
    - Coin
    - Food
    - Weapon???
    - Tool???
    - ???
- Assailant
    - Monster???
- Animal
    - Dog
    - ???
- Dimension Gate (Moves the player to a different tile or Dimension)
- Trigger (Triggers an event)


**Properties**

- *Killable* - Entities can destroy this tile.
- *Solid* - Entities can not pass through this tile.
- *Items* - Items that the entity drops after it is destroyed.


## Tile list

**Tiles**

- Air
- Water
    - Liquid
- Stone
    - Solid
    - Destroyable
- Wood
    - Solid
    - Destroyable
    - Items


**Properties**

- *Destroyable* - Entities can destroy this tile.
- *Liquid* - Can be obtained as a liquid.
- *Solid* - Entities can not pass through this tile.


## Menu list

- Game main
- Game minigame
- Main menu


## Environment

- Sun rotation
- Shade
- Nighttime


## Dreaming

A dream is temporary dimension.
A dream can be a nightmare were assailants form a challenge.


## Core mechanics

- Game (Game is made out of 1 or more Worlds)
- Entity (The player, animals, assailants are all enitities)
- Events (Events can be trigger that execute based on time or )
- Triggers (Triggers can start an event)
- Dimension (Triggers, events, entities, and tiles are contents of a dimension)
- Game clock (Timeline of the players adventure)
- Tile (What a dimension is made out of)
- Save game
- Menus / State
- Minigames???
- Crafting
- Minimap (Displays waypoints & entities)
- FogOfWar
- Renderer (Renders the picture of the user)
- Camera (Generally follows player)
- Sleeping
- Time anchors
- Modding


## Multiplayer

Server
Socket


## Save game

**XML format**

- Game
    - dimension
        - Map
            - Tile
            - Trigger
        - Entity
        - Event

**XML sample**

```
<game>
    <dimensions>
        <dimension id="0">
            <time>1274304000</time>
            <worlds>
                <world id="0">
                    <map>
                        <tile id="0">
                        </tile>
                        ...
                        <block id="0">
                        </block>
                        ...
                    </map>
                    <entities>
                        <entity id="0" name="Bas">
                            <objectives>
                                <objective>
                                </objective>
                                ...
                            </objectives>
                            <inventory slots="8">
                                <item position="0" item-id="43" />
                                <item position="1" item-id="54" />
                                <item position="2" item-id="83" />
                                ...
                            </inventory>
                        </entity>
                        <entity id="1" name="Assailant">
                            <inventory slots="1">
                                <item position="0" item-id="43" />
                            </inventory>
                        </entity>
                        ...
                    </entities>
                    ...
                </world>
            </worlds>
            <events>
                <event id="0">
                </event>
                ...
            </events>
        </dimension>
        ...
    </dimensions>
</game>
```
