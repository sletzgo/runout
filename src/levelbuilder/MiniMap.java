package levelbuilder;

import static java.awt.RenderingHints.*;
import static util.RadiansUtil.directionToHorizontalVelocity;
import static util.RadiansUtil.directionToVerticalVelocity;
import static util.ImageResource.toBufferedImage;
import static util.GraphicsUtil.*;

import javax.swing.JPanel;
import javax.swing.Timer;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Font;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import java.awt.image.BufferedImage;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import game.Entity;
import game.Game;
// import game.GameCursor;
import game.ResourcePath;
import game.Sprite;
import game.Terrain;
import game.Tile;
import game.World;
import game.WorldObject;
import game.WorldPosition;
import game.WorldSeason;
import game.WorldTime;
import game.WorldType;
import game.Chunk;
import util.ImageResource;

/**
 * Map
 * 
 * @since 0.1.0
 * @author Lawrence Bensaid <lawrencebensaid@icloud.com>
 */
@SuppressWarnings("serial")
public class MiniMap extends JPanel implements MouseListener, MouseMotionListener, MouseWheelListener, KeyListener, ActionListener {

    public static ImageResource water = new ImageResource("terrain/water.gif");
    public static ImageResource grass = new ImageResource("terrain/grass.jpg");
    public static ImageResource iconPlusCircle = new ImageResource("ui/iconPlusCircle.png");

    public static final int ZOOM_DEFAULT = 8;

    public static final int KEY_RIGHT = 151;
    public static final int KEY_LEFT = 149;
    public static final int KEY_UP = 150;
    public static final int KEY_DOWN = 152;
    public static final int KEY_W = 87;
    public static final int KEY_A = 65;
    public static final int KEY_S = 83;
    public static final int KEY_D = 68;
    public static final int KEY_SHIFT = 16;
    public static boolean[] keys = new boolean[256];

    private static final String version = Game.version.toString();

    private boolean showGrid = true;
    private boolean showRenderer = false;
    private boolean focus = false;
    private int fontSize = 12;
    private int textPadding = 8;
    private int zoom = ZOOM_DEFAULT;
    private ResourcePath tileBrush = new ResourcePath("tile", "Water");
    private ResourcePath objectBrush = new ResourcePath("object", "Lantern");
    private ResourcePath entityBrush = new ResourcePath("entity", "Assailant");
    private int mouseX = 0;
    private int mouseY = 0;
    private int dx = 0;
    private int dy = 0;
    private long sx = 0;
    private long sy = 0;
    private long translateX = 0;
    private long translateY = 0;
    private BuildMode buildMode = BuildMode.TERRAIN;
    private Color cursorColor = new Color(0, 255, 0, 128);
    private Color bgActive;
    private Color bgInactive;
    private World world;
    // private GameCursor cursor = new GameCursor();
    private String notLoadedMessage = "No world loaded";
    private boolean canEdit;
    private boolean showVersion;
    private boolean showTime;

    // Temp
    private float fade = 1f;

    public MiniMap() {
        this(false);
    }

    public MiniMap(boolean canEdit) {
        this(canEdit, false);
    }

    public MiniMap(boolean canEdit, boolean showTime) {
        this(canEdit, showTime, false);
    }

    public MiniMap(boolean canEdit, boolean showTime, boolean showVersion) {
        this.canEdit = canEdit;
        this.showTime = showTime;
        this.showVersion = showVersion;
        load();
        setFocusable(true);
        addKeyListener(this);
        addMouseListener(this);
        addMouseMotionListener(this);
        addMouseWheelListener(this);
        Timer timer = new Timer(50, this);
        timer.start();
    }

    /**
     * The message that will appear on the screen if nothing is loaded.
     * 
     * @param message Message.
     */
    public void setNotLoadedMessage(String message) {
        notLoadedMessage = message;
        repaint();
    }

    /**
     * Setting for displaying the renderer border / padding.
     * 
     * @param state whether or not the setting is enabled.
     */
    public void showRenderer(boolean state) {
        showRenderer = state;
        repaint();
    }

    /**
     * Setting for displaying the grid overlay as ESP.
     * 
     * @param state whether or not the setting is enabled.
     */
    public void showGrid(boolean state) {
        showGrid = state;
        repaint();
    }

    /**
     * Change the brush of your cursor.
     * 
     * @see game.Terrain#loadResourcesIfNeeded
     * @param tileID Tile identifier.
     */
    public void setTileBrush(ResourcePath tileID) {
        System.out.println("Brush: " + tileID);
        tileBrush = tileID;
        repaint();
    }

    /**
     * Change the brush of your cursor.
     * 
     * @param objectID Object identifier.
     */
    public void setObjectBrush(ResourcePath objectID) {
        System.out.println("Brush: " + objectID);
        objectBrush = objectID;
        repaint();
    }

    /**
     * Change the brush of your cursor.
     * 
     * @param entityID Entity identifier.
     */
    public void setEntityBrush(ResourcePath entityID) {
        System.out.println("Brush: " + entityID);
        entityBrush = entityID;
        repaint();
    }

    /**
     * Moves the view to a new tile.
     * 
     * @param gtx Global tile X coördinate.
     * @param gty Global tile Y coördinate.
     */
    public void goTo(long gtx, long gty) {
        translateX = gtx;
        translateY = gty;
        repaint();
    }

    /**
     * Resets the zoom value.
     */
    public void setZoom() {
        setZoom(ZOOM_DEFAULT);
    }

    /**
     * Sets the zoom to a new value.
     * 
     * @param zoom value.
     */
    public void setZoom(int zoom) {
        if (zoom > 0) {
            int tZoom = zoom - this.zoom;
            translateX = translateX - (getMouseTileX() * tZoom);
            translateY = translateY - (getMouseTileY() * tZoom);
            this.zoom = zoom;
            repaint();
        }
    }

    /**
     * Increments the zoom value by 1.
     */
    public void zoomIn() {
        setZoom(zoom + 1);
    }

    /**
     * Decrements the zoom value by 1.
     */
    public void zoomOut() {
        setZoom(zoom - 1);
    }

    /**
     * @return current loaded world. If no world is loaded {@code null} will be
     *         returned.
     */
    public World getLoadedWorld() {
        return world;
    }

    /**
     * Unloads the world.
     */
    public void unload() {
        world = null;
        repaint();
    }

    /**
     * Loads the first world if no world is already loaded. If a world is already
     * loaded the world will simply be reloaded.
     */
    public void load() {
        if (world != null) {
            load(world.getName());
            return;
        }
        for (game.Dimension dimension : Game.getDimensions()) {
            World[] worlds = dimension.getWorlds();
            if (worlds.length > 0) {
                load(worlds[0].getName());
                return;
            }
        }
        System.out.println("Unable to find a world!");
    }

    /**
     * Loads the world.
     * 
     * @param worldName name of the world you want to load.
     */
    public void load(String worldName) {
        world = Game.getWorld(worldName);
        if (world != null && world.getType() == WorldType.HELLISH) {
            bgActive = new Color(96, 0, 0);
            bgInactive = new Color(64, 0, 0);
        } else {
            bgActive = Color.darkGray;
            bgInactive = Color.black;
        }
        repaint();
    }

    /**
     * Sets the mode and corresponding cursor color.
     * 
     * @param mode Build mode.
     */
    public void setMode(BuildMode mode) {
        this.buildMode = mode;
        switch (mode) {
        case TERRAIN:
            cursorColor = new Color(0, 255, 0, 128);
            break;
        case ENTITY:
            cursorColor = new Color(0, 255, 0, 128);
            break;
        case OBJECT:
            cursorColor = new Color(0, 255, 255, 128);
            break;
        case TRIGGER:
            cursorColor = new Color(255, 255, 0, 128);
            break;
        }
        repaint();
    }

    private void select() {
        WorldPosition position = new WorldPosition(getMouseTileX(), getMouseTileY(), getLoadedWorld().getName());
        for (WorldObject object : world.getObjects()) {
            if (WorldPosition.getDistance(object.getPosition(), position) <= 1) {
                object.setSelected();
                break;
            }
        }
    }

    /**
     * Right-click
     */
    private void unaction() {
        unaction(false);
    }

    /**
     * Right-click
     * 
     * @param drag whether or not the mouse button is held down.
     */
    private void unaction(boolean drag) {
        if (!canEdit) {
            return;
        }
        WorldPosition position = new WorldPosition(getMouseTileX(), getMouseTileY(), getLoadedWorld().getName());
        switch (buildMode) {
        case TERRAIN:
            int cmx = Chunk.getChunkCordAt(getMouseTileX());
            int cmy = Chunk.getChunkCordAt(getMouseTileY());
            world.getTerrain().removeChunk(cmx, cmy);
            break;
        case ENTITY:
            if (!drag)
                for (Entity entity : world.getEntities()) {
                    if (WorldPosition.getDistance(entity.getPosition(), position) <= 1) {
                        world.despawn(entity);
                        break;
                    }
                }
            break;
        case OBJECT:
            if (!drag)
                for (WorldObject object : world.getObjects()) {
                    if (WorldPosition.getDistance(object.getPosition(), position) <= 1) {
                        world.despawn(object);
                        break;
                    }
                }
            break;
        case TRIGGER:
            break;
        }
        repaint();
    }

    /**
     * Left-click
     */
    private void action() {
        action(false);
    }

    /**
     * Left-click
     * 
     * @param drag whether or not the mouse button is held down.
     */
    private void action(boolean drag) {
        if (!canEdit) {
            return;
        }
        int cmx = Chunk.getChunkCordAt(getMouseTileX());
        int cmy = Chunk.getChunkCordAt(getMouseTileY());
        WorldPosition position = new WorldPosition(getMouseTileX(), getMouseTileY(), getLoadedWorld().getName());
        if (world.getTerrain().getChunkAt(cmx, cmy) == null) {
            world.getTerrain().addChunk(new Chunk(cmx, cmy, tileBrush));
            return;
        }
        switch (buildMode) {
        case TERRAIN:
            world.getTerrain().getChunkAt(cmx, cmy).setLocalTileAt(tileBrush, Chunk.getLocalCordAt(getMouseTileX()),
                    Chunk.getLocalCordAt(getMouseTileY()));
            break;
        case ENTITY:
            if (!drag)
                world.spawn(Entity.create(entityBrush, position, "Entity"));
            break;
        case OBJECT:
            if (!drag)
                world.spawn(WorldObject.create(objectBrush, position));
            break;
        case TRIGGER:
            select();
            break;
        }
        repaint();
    }

    public long getViewX() {
        return translateX + getWidth() / 2;
    }

    public long getViewY() {
        return translateY + getWidth() / 2;
    }

    public int getCordX() {
        return (int) (mouseX - getViewX());
    }

    public int getCordY() {
        return (int) (mouseY - getViewY());
    }

    public int getMouseTileX() {
        return Math.floorDiv((getCordX() + zoom / 2), zoom);
    }

    public int getMouseTileY() {
        return Math.floorDiv((getCordY() + zoom / 2), zoom);
    }

    public int getMouseChunkX() {
        return Math.floorDiv((Chunk.getChunkCordAt(getCordX()) + zoom / 2), zoom);
    }

    public int getMouseChunkY() {
        return Math.floorDiv((Chunk.getChunkCordAt(getCordY()) + zoom / 2), zoom);
    }

    public int getRelativeMouseX() {
        return getRelativeMouseX(1);
    }

    public int getRelativeMouseX(double multiplier) {
        return (int) (getMouseTileX() * zoom * multiplier) - zoom / 2;
    }

    public int getRelativeMouseY() {
        return getRelativeMouseY(1);
    }

    public int getRelativeMouseY(double multiplier) {
        return (int) (getMouseTileY() * zoom * multiplier) - zoom / 2;
    }

    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        Graphics2D g2 = (Graphics2D) g;
        g2.setRenderingHint(KEY_ANTIALIASING, VALUE_ANTIALIAS_ON);
        g2.setRenderingHint(KEY_TEXT_ANTIALIASING, VALUE_TEXT_ANTIALIAS_ON);

        if (world == null) {
            int width = getWidth();
            int height = getHeight();
            int size = width < height ? width : height;
            Font font = new Font("Helvetica Neue", Font.PLAIN, size / 10);
            drawCenteredString(g2, notLoadedMessage, new Rectangle(width, height), font);
            return;
        }

        // Draw board
        if (focus) {
            g2.setColor(bgActive);
        } else {
            g2.setColor(bgInactive);
        }
        g2.fillRect(0, 0, getWidth(), getHeight());

        g2.translate(getViewX(), getViewY());

        // Draw chunks
        int paddingX = showRenderer ? (int) (getWidth() / 8) : 0;
        int paddingY = showRenderer ? (int) (getHeight() / 8) : 0;
        g2.setColor(Color.red);
        for (Chunk chunk : world.getTerrain().getChunks()) {
            int x = (int) chunk.getX() * zoom;
            int y = (int) chunk.getY() * zoom;
            int chunkX = x * Chunk.SIZE;
            int chunkY = y * Chunk.SIZE;
            int chunkSize = zoom * Chunk.SIZE;
            if (showGrid)
                g2.drawRect(chunkX - zoom / 2, chunkY - zoom / 2, chunkSize, chunkSize);
            for (int cx = 0; cx < Chunk.SIZE; cx++) {
                for (int cy = 0; cy < Chunk.SIZE; cy++) {
                    Tile tile = chunk.getLocalTileAt(cx, cy);
                    if (tile == null) {
                        continue;
                    }
                    int tileX = (chunkX + cx * zoom) - zoom / 2;
                    int tileY = (chunkY + cy * zoom) - zoom / 2;
                    long top = -getViewY() + paddingY - zoom;
                    long bottom = (-getViewY() + getHeight()) - paddingY;
                    long left = -getViewX() + paddingX - zoom;
                    long right = (-getViewX() + getWidth()) - paddingX;
                    if (left < tileX && right > tileX && top < tileY && bottom > tileY) {
                        Sprite sprite = tile.getSprite(chunk);
                        g2.drawImage(sprite.getImage(), tileX, tileY, zoom, zoom, null);
                    } else if (showGrid) {
                        g2.drawRect(tileX, tileY, zoom, zoom);
                    }
                }
            }
        }
        if (showRenderer) {
            int width = getWidth() - paddingX * 2;
            int height = getHeight() - paddingY * 2;
            g2.drawRect((int) (-getViewX() + paddingX), (int) (-getViewY() + paddingY), width, height);
        }

        // Cursor
        if (canEdit) {
            int cmx = Chunk.getChunkCordAt(getMouseTileX());
            int cmy = Chunk.getChunkCordAt(getMouseTileY());
            if (world.getTerrain().getChunkAt(cmx, cmy) == null) {
                g2.setColor(new Color(128, 128, 128, 64));
                int x = cmx * Chunk.SIZE * zoom - zoom / 2;
                int y = cmy * Chunk.SIZE * zoom - zoom / 2;
                int size = Chunk.SIZE * zoom / 2;
                g2.fillRect(x, y, Chunk.SIZE * zoom, Chunk.SIZE * zoom);
                g2.setPaint(new Color(128, 128, 128));
                BufferedImage image = toBufferedImage(iconPlusCircle.getImage());
                g2.drawImage(image, x + size / 2, y + size / 2, size, size, null);
                g2.setFont(new Font("Helvetica Neue", Font.PLAIN, zoom));
                g2.drawString("Generate chunk", x + zoom / 2, y + Chunk.SIZE * zoom - zoom / 2);
            } else {
                g2.setColor(cursorColor);
                g2.fillRect(getRelativeMouseX(), getRelativeMouseY(), zoom, zoom);
                int iconSize = 16;
                int x = getRelativeMouseX() + zoom - iconSize / 2;
                int y = getRelativeMouseY() - iconSize / 2;
                g2.setColor(Color.darkGray);
                g2.fillRect(x - 1, y - 1, iconSize + 2, iconSize + 2);
                g2.drawImage(Terrain.getTile(tileBrush, 0, 0).getIcon(), x, y, iconSize, iconSize, null);
                g2.setColor(new Color(0, 255, 255, (int) (fade * 255)));
                g2.setFont(new Font("Helvetica Neue", Font.BOLD, fontSize));
                g2.drawString(tileBrush.getName(), (int) (x + iconSize * 1.5), (int) (y + iconSize / 1.5));
            }
        }

        // Draw objects
        for (WorldObject object : world.getObjects()) {
            int x = (int) object.getPosition().getX() * zoom;
            int y = (int) object.getPosition().getY() * zoom;
            int ix = x - zoom / 2;
            int iy = y - zoom / 2;
            int width = (int) (object.getWidth() * zoom);
            int height = (int) (object.getHeight() * zoom);
            if (object.getSelected()) {
                String text = object.getResourcePath().toString(false);
                g2.setColor(Color.cyan);
                Rectangle rectangle = new Rectangle(ix, iy - fontSize * 2, width, fontSize);
                drawCenteredString(g2, text, rectangle, new Font("Helvetica Neue", Font.BOLD, fontSize));
            }
            Sprite sprite = object.getSprite();
            g2.drawImage(sprite.getImage(), ix, iy, width, height, null);
        }

        // Draw entities
        for (Entity entity : world.getEntities()) {
            int x = (int) entity.getPosition().getX() * zoom;
            int y = (int) entity.getPosition().getY() * zoom;
            float rotation = entity.getPosition().getRotation();
            int size = (int) (zoom / 1.5);
            String postfix = "";
            if (entity.getResourcePath().equals("entity.Player")) {
                if (!((game.entity.Player) entity).isConnected()) {
                    g2.setColor(Color.white);
                    postfix = "(disconnected)";
                }
            } else {
                g2.setColor(Color.green);
            }
            g2.setFont(new Font("Helvetica Neue", Font.PLAIN, fontSize));
            g2.drawString(entity.getName() + " " + postfix, x + fontSize, y + size / 3);
            g2.drawOval(x - size / 2, y - size / 2, size, size);
            g2.drawLine(x, y, x + (int) directionToHorizontalVelocity(rotation, size / 2),
                    y + (int) directionToVerticalVelocity(rotation, size / 2));
        }

        g2.translate(-getViewX(), -getViewY());

        if (showVersion) {
            // Version
            g2.setColor(Color.white);
            g2.setFont(new Font("Helvetica Neue", Font.BOLD + Font.ITALIC, fontSize));
            g2.drawString("Runout© Level editor - " + version, textPadding, textPadding + fontSize / 2);
        }

        if (showTime) {
            // Draw sim & time
            g2.setColor(Color.white);
            int statsFontSize = 12;
            int statsPadding = 4;
            g2.setFont(new Font("Helvetica Neue", Font.BOLD, statsFontSize));
            g2.drawString("sim " + Game.getSIM(), statsPadding, getHeight() - statsPadding - (statsFontSize * 4));
            WorldTime time = world.getTime();
            if (time != null) {
                g2.drawString("time " + time.getClock(), statsPadding, getHeight() - statsPadding - statsFontSize);
                g2.drawString("day " + time.getDayOfYear(), statsPadding,
                        getHeight() - statsPadding - (statsFontSize * 3));
                WorldSeason season = time.getSeason();
                g2.setColor(season.getUIColor());
                g2.drawString("season " + season.getName(), statsPadding, getHeight() - statsPadding);
                g2.setColor(Color.white);
                g2.drawString("year " + time.getYear(), statsPadding, getHeight() - statsPadding - (statsFontSize * 2));
            }
        } else {
            // Stats
            g2.setColor(Color.cyan);
            g2.setFont(new Font("Helvetica Neue", Font.BOLD, fontSize));
            g2.drawString("World: " + world.getName(), textPadding, getHeight() - textPadding - (fontSize * 4));
            g2.drawLine(textPadding, (int) (getHeight() - textPadding * 1.5 - fontSize * 3), textPadding + 100,
                    (int) (getHeight() - textPadding * 1.5 - fontSize * 3));
            g2.drawString("Biome: " + world.getTerrain().getBiome(0, 0).getName(), textPadding,
                    getHeight() - textPadding - fontSize * 2);
            g2.drawString("Tile: x " + getMouseTileX() + " y " + getMouseTileY() + " z: 0", textPadding,
                    getHeight() - textPadding - fontSize);
            g2.drawString(
                    "Chunk: x " + Chunk.getChunkCordAt(getMouseTileX()) + " y " + Chunk.getChunkCordAt(getMouseTileY()),
                    textPadding, getHeight() - textPadding);
        }
    }

    @Override
    public void mouseDragged(MouseEvent e) {
        mouseX = e.getX();
        mouseY = e.getY();
        if (e.isMetaDown()) {
            cursorColor = new Color(255, 0, 0, 128);
            repaint();
        } else {
            setMode(buildMode);
        }
        if (!keys[KEY_SHIFT]) {
            translateX = sx + (e.getX() - dx);
            translateY = sy + (e.getY() - dy);
            repaint();
        }
    }

    @Override
    public void mouseMoved(MouseEvent e) {
        mouseX = e.getX();
        mouseY = e.getY();
        if (e.isMetaDown()) {
            cursorColor = new Color(255, 0, 0, 128);
            repaint();
        } else {
            setMode(buildMode);
        }
        if (keys[KEY_SHIFT]) {
            setCursor(Cursor.getPredefinedCursor(Cursor.CROSSHAIR_CURSOR));
            if (e.isMetaDown()) {
                unaction(true);
            } else {
                action(true);
            }
        } else {
            setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        }
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        if (e.isMetaDown()) {
            unaction();
        } else {
            action();
        }
    }

    @Override
    public void mousePressed(MouseEvent e) {
        sx = translateX;
        sy = translateY;
        dx = e.getX();
        dy = e.getY();
        if (e.isMetaDown()) {
            cursorColor = new Color(255, 0, 0, 128);
            repaint();
        } else {
            setMode(buildMode);
        }
        requestFocus();
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        if (e.isMetaDown()) {
            cursorColor = new Color(255, 0, 0, 128);
            repaint();
        } else {
            setMode(buildMode);
        }
    }

    @Override
    public void mouseEntered(MouseEvent e) {

    }

    @Override
    public void mouseExited(MouseEvent e) {

    }

    @Override
    public void mouseWheelMoved(MouseWheelEvent e) {
        int notches = e.getWheelRotation();
        setZoom(zoom + notches);
    }

    @Override
    public void keyTyped(KeyEvent e) {

    }

    @Override
    public void keyPressed(KeyEvent e) {
        keys[e.getKeyCode()] = true;
        if (keys[KeyEvent.VK_OPEN_BRACKET]) {
            int ID = Terrain.resourcePathToTileID(tileBrush);
            ID = Tile.previous(ID);
            ResourcePath resourcePath = Terrain.tileIDToResourcePath(ID);
            if (resourcePath != null) {
                tileBrush = resourcePath;
                fade = 1f;
                repaint();
            }
        }
        if (keys[KeyEvent.VK_CLOSE_BRACKET]) {
            int ID = Terrain.resourcePathToTileID(tileBrush);
            ID = Tile.next(ID);
            ResourcePath resourcePath = Terrain.tileIDToResourcePath(ID);
            if (resourcePath != null) {
                tileBrush = resourcePath;
                fade = 1f;
                repaint();
            }
        }
    }

    @Override
    public void keyReleased(KeyEvent e) {
        keys[e.getKeyCode()] = false;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (fade > 0) {
            fade -= 0.05;
            repaint();
        }
    }

}