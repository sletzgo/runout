package levelbuilder;

public enum BuildMode {
    TERRAIN,
    ENTITY,
    OBJECT,
    TRIGGER
}