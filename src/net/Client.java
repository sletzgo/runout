package net;

import static util.BufferUtil.*;

import java.io.IOException;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.net.Socket;
import java.util.ArrayList;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

import game.Entity;
import game.Game;
import game.World;
import game.WorldPosition;
import game.entity.Player;

/**
 * Client
 */
public class Client {

	public static Socket sckt;
	private static DataInputStream dtinpt;
	private static DataOutputStream dtotpt;
	public static World loadedWorld = null;

	public static void connect(String host) {
		connect(host, Server.DEFAULT_PORT);
	}

	public static void connect(String host, int port) {
		connect("Client", host, port);
	}

	public static void connect(String name, String host, int port) {
		Thread net = new Thread() {
			@Override
			public void run() {
				super.run();
				try {
					sckt = new Socket(host, port);
					dtinpt = new DataInputStream(sckt.getInputStream());
					dtotpt = new DataOutputStream(sckt.getOutputStream());
					out("authName:" + name);
					ArrayList<Byte> msgin = new ArrayList<Byte>();
					int blen = 0;
					boolean exit = false;
					while (!exit) {
						blen = dtinpt.readInt();
						System.out.println("Receiving buffer of length " + blen);
						for (int i = 0; i < blen; i++) {
							msgin.add(dtinpt.readByte());
						}
						Byte[] barr = msgin.toArray(new Byte[msgin.size()]);
						Object obj = bytesToObject(barr);
						if (obj.equals("exit")) {
							exit = true;
						}
						in(obj);
					}
				} catch (Exception e) {
					System.out.println("[ERROR] >> " + e.getMessage());
					System.exit(0);
				}
			}
		};
		net.setName("core.net");
		net.start();
	}

	@SuppressWarnings("unused")
	private static void in(Object object) {
		System.out.println(object);
		String message = (String) object;
		String[] pairs = message.split(";");
		boolean data = false;
		Entity newEntity = null;
		// TODO: Lawrence > I really want to get this code refactored ASAP.
		for (String pair : pairs) {
			String[] keyValue = pair.split(":");
			if (keyValue[0].equals("setting.fog")) {
				data = true;
				String value = keyValue[1].toLowerCase();
				if (value == "true") {
					JOptionPane.showMessageDialog(new JFrame(), "Fog of war has been enabled by Server");
				} else {
					JOptionPane.showMessageDialog(new JFrame(), "Fog of war has been disabled by Server");
				}
			}
			if (keyValue[0].equals("entitySpawn")) {
				data = true;
				String value = keyValue[1];
				newEntity = new Entity(value, new WorldPosition(0, 0, "OverWorld"));
			}
			if (keyValue[0].equals("entityX")) {
				data = true;
				String value = keyValue[1];
				if (newEntity == null) {
					continue;
				}
				newEntity.setX(Float.parseFloat(value));
			}
			if (keyValue[0].equals("entityY")) {
				data = true;
				String value = keyValue[1];
				if (newEntity == null) {
					continue;
				}
				newEntity.setY(Float.parseFloat(value));
			}
			if (keyValue[0].equals("entityType")) {
				data = true;
				String value = keyValue[1];
				if (newEntity == null) {
					continue;
				}
				switch (value) {
				case "Player":
					Game.spawn((Player) newEntity);
					break;
				default:
					Game.spawn(newEntity);
					break;
				}
				newEntity.setY(Float.parseFloat(value));
			}
		}
		// if (!data) {
		// System.out.println(object);
		// }
	}

	public static void out(Object object) {
		try {
			byte[] buffer = objectToBytes(object);
			dtotpt.writeInt(buffer.length);
			dtotpt.write(buffer);
		} catch (IOException exception) {
			exception.printStackTrace();
		}
	}

	public static World getLoadedWorld() {
		return loadedWorld;
	}

}