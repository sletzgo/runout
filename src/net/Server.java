package net;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.UUID;

import game.Game;
import gui.ServerGUI;

/**
 * Server
 */
public class Server {

    public static ServerSocket ssckt;
    private static boolean listenForClients = false;
    private static ArrayList<ServerClient> clients = new ArrayList<ServerClient>();

    // Constants
    public static final int DEFAULT_PORT = 9876;

    public static void start() {
        try {
            ssckt = new ServerSocket(DEFAULT_PORT);
            print("net online!");
            Game.start();
            listenForClients = true;
            while (listenForClients) {
                Socket sckt = Server.ssckt.accept();
                new ServerClient(sckt);
            }
        } catch (IOException exception) {
            print("start error: " + exception.getMessage());
        }
    }

    public static void addClient(ServerClient client) {
        clients.add(client);
        print("Client '" + client.getName() + "' connected!");
        ServerGUI.instance.updateClients();
    }

    public static void removeClient(ServerClient client) {
        removeClient(client.getID());
    }

    public static void removeClient(UUID id) {
        for (int i = 0; i < clients.size(); i++) {
            ServerClient client = clients.get(i);
            if (client.getID().equals(id)) {
                clients.remove(i);
                print("Client '" + client.getName() + "' disconnected!");
                ServerGUI.instance.updateClients();
                return;
            }
        }
    }

    public static void broadcast(String message) {
        for (ServerClient client : clients) {
            client.out(message);
        }
    }

    public static ArrayList<ServerClient> getClients() {
        return clients;
    }

    public static void print(Object message) {
        print("Server", message);
    }

    public static void print(String agent, Object message) {
        System.out.println(message);
        if (ServerGUI.instance != null) {
            ServerGUI.instance.terminalView.print(agent, message);
        }
    }

}