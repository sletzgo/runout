package net;

import static util.BufferUtil.*;

import java.io.EOFException;
import java.io.IOException;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.net.Socket;
import java.util.UUID;
import java.util.ArrayList;

import game.Game;
import game.WorldObject;
import game.WorldPosition;
import game.entity.Player;
import gui.ServerGUI;

/**
 * ServerClient
 */
public class ServerClient {

    private Socket sckt;
    private DataInputStream dtinpt;
    private DataOutputStream dtotpt;
    private String name;
    private final UUID ID;

    public ServerClient(Socket sckt) {
        this(sckt, UUID.randomUUID());
    }

    public ServerClient(Socket sckt, UUID ID) {
        this(sckt, ID, "Client");
    }

    public ServerClient(Socket sckt, UUID ID, String name) {
        this.sckt = sckt;
        this.ID = ID;
        this.name = name;
        Thread thread = new Thread() {
            @Override
            public void run() {
                init();
            }
        };
        thread.setName("ServerClient_" + ID);
        thread.start();
    }

    private void init() {
        ArrayList<Byte> msgin = new ArrayList<Byte>();
        int blen = 0;
        try {
            dtinpt = new DataInputStream(sckt.getInputStream());
            dtotpt = new DataOutputStream(sckt.getOutputStream());
            boolean exit = false;
            while (!exit) {
                blen = dtinpt.readInt();
                net.Server.print("Receiving buffer of length " + blen);
                for (int i = 0; i < blen; i++) {
                    msgin.add(dtinpt.readByte());
                }
                Byte[] barr = msgin.toArray(new Byte[msgin.size()]);
                Object obj = bytesToObject(barr);
                if (obj.equals("exit")) {
                    exit = true;
                }
                in(obj);
            }
            // Server.removeClient(this);
        } catch (EOFException exception) {
            Server.removeClient(this);
        } catch (IOException exception) {
            net.Server.print("init error: " + exception.getMessage());
        }
    }

    private void auth() {
        Server.addClient(this);
        // TODO: Send world to client
        WorldObject gate = Game.getRandomGate();
        if (gate == null) {
            net.Server.print("Player could not spawn because there are no gates in the world to spawn at!");
            return;
        }
        WorldPosition gatePosition = gate.getPosition();
        WorldPosition spawnPoint = new WorldPosition(gatePosition.getX(), gatePosition.getY() + 2,
                gatePosition.getWorldName());
        Game.spawn(new Player(spawnPoint, name));
    }

    public void in(Object object) {
        String message = (String) object;
        String[] pairs = message.split(";");
        // boolean data = false;
        for (String pair : pairs) {
            String[] keyValue = pair.split(":");
            if (keyValue[0].equals("displayName")) {
                // data = true;
                String displayName = keyValue[1];
                net.Server.print("" + name + "'s' new name is '" + displayName + "'");
                name = displayName;
                ServerGUI.instance.updateClients();
            }
            if (keyValue[0].equals("authName")) {
                // data = true;
                String displayName = keyValue[1];
                name = displayName;
                auth(); // Declare that the client has connected
            }
        }
        // if (!data) {
            net.Server.print(name, object);
        // }
    }

    public void out(Object object) {
        try {
            byte[] buffer = objectToBytes(object);
            dtotpt.writeInt(buffer.length);
            dtotpt.write(buffer);
        } catch (IOException | NullPointerException exception) {
        }
    }

    public UUID getID() {
        return ID;
    }

    public String getName() {
        return name;
    }

    public void disconnect() {
        if (!sckt.isClosed()) {
            try {
                dtinpt.close();
                dtotpt.close();
                sckt.close();
                Server.removeClient(this);
            } catch (IOException exception) {
                net.Server.print("dis error: " + exception.getMessage());
            }
        }
    }

    @Override
    public String toString() {
        String host = sckt.getInetAddress().getHostAddress();
        return name + " (" + host + ")";
    }

}