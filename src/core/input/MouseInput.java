package core.input;

import com.jogamp.newt.event.MouseEvent;
import com.jogamp.newt.event.MouseListener;

import core.Window;

/**
 * @since 0.1.0
 * @author Lawrence Bensaid <lawrencebensaid@icloud.com>
 */
public class MouseInput implements MouseListener {

    private static int x = 0;
    private static int y = 0;
    private static boolean exists = false;

    public static int getX() {
        return x / 2;
    }

    public static int getY() {
        return y / 2;
    }

    public static boolean mouseExists() {
        return MouseInput.exists;
    }

    public static int getWorldX() {
        return ((int) Window.camera.getViewport() / Window.getWidth() * x - (int) Window.camera.getViewport() / 2)
                + (int) Window.camera.getX();
    }

    public static int getWorldY() {
        float unitsTall = Window.camera.getViewport() * (Window.getHeight() / Window.getWidth());
        return (int) ((unitsTall / Window.getHeight() * y - unitsTall / 2) + Window.camera.getY());
    }

    @Override
    public void mouseClicked(MouseEvent arg0) {

    }

    @Override
    public void mouseDragged(MouseEvent arg0) {

    }

    @Override
    public void mouseEntered(MouseEvent arg0) {

    }

    @Override
    public void mouseExited(MouseEvent arg0) {

    }

    @Override
    public void mouseMoved(MouseEvent event) {
        if (!MouseInput.exists) {
            MouseInput.exists = true;
        }
        x = event.getX();
        y = event.getY();
    }

    @Override
    public void mousePressed(MouseEvent arg0) {

    }

    @Override
    public void mouseReleased(MouseEvent arg0) {

    }

    @Override
    public void mouseWheelMoved(MouseEvent arg0) {

    }

}