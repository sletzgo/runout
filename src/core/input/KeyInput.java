package core.input;

import com.jogamp.newt.event.KeyEvent;
import com.jogamp.newt.event.KeyListener;

/**
 * @since 0.1.0
 * @author Bas Schimmel <s1143474@student.windesheim.nl>
 */
public class KeyInput implements KeyListener {
	
	public static final int RIGHT = 151;
	public static final int LEFT = 149;
	public static final int UP = 150;
	public static final int DOWN = 152;

	public static final int W = 87;
	public static final int A = 65;
	public static final int S = 83;
	public static final int D = 68;
	
	public static final int SPACE = 32;
	
	private static boolean[] keys = new boolean[256];
	
	@Override
	public void keyPressed(KeyEvent event) {
		keys[event.getKeyCode()] = true;
		// System.out.println(event.getKeyCode());
	}

	@Override
	public void keyReleased(KeyEvent event) {
		keys[event.getKeyCode()] = false;
	}
	
	public static boolean getKey (int keyCode) {
		return keys[keyCode];
	}

}
