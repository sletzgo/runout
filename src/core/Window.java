package core;

import com.jogamp.newt.opengl.GLWindow;
import com.jogamp.opengl.GLProfile;

import core.input.KeyInput;
import core.input.MouseInput;
import game.Camera;
import game.WorldPosition;

import com.jogamp.opengl.GLCapabilities;

/**
 * @author Lawrence Bensaid <lawrencebensaid@icloud.com>
 */
public class Window {
	
	private static GLProfile profile = null;
	private static GLWindow window = null;
	private static final int WIDTH = 640;
	private static final int HEIGHT = 360;

	public static Camera camera = new Camera(new WorldPosition(0, 0, "Overworld"));
	
	public static void init() {
		GLProfile.initSingleton();
		profile = GLProfile.get(GLProfile.GL2);
		GLCapabilities caps = new GLCapabilities(profile);
		
		window = GLWindow.create(caps);
		window.setSize(WIDTH, HEIGHT);
		window.addGLEventListener(new Renderer());
		window.addKeyListener(new KeyInput());
		window.addMouseListener(new MouseInput());
		window.setTitle("Runout");
		
		// FPSAnimator animator = new FPSAnimator(window, 60);
		// animator.start();
		
		// window.setFullscreen(true);
		window.setVisible(true);
	}
	
	public static int getWidth() {
		return window.getWidth();
	}
	
	public static int getHeight() {
		return window.getHeight();
	}
	
	public static GLProfile getProfile() {
		return profile;
	}

	public static void render() {
		if (window == null) {
			return;
		}
		window.display();
	}
	
}