package core.io;

import static util.Format.boolToString;

import java.awt.Color;
import java.io.File;
import java.util.ArrayList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import game.Chunk;
import game.Dimension;
import game.Entity;
import game.Event;
import game.Game;
import game.Tile;
import game.World;
import game.WorldObject;
import game.WorldPosition;
import game.WorldSeason;
import game.ResourcePath;
import game.Terrain;
import game.WorldTime;
import game.WorldType;

public class GameFile {

	public static void save(final String file) {
		if (file.endsWith(".rlvl")) {
			save(new File(file));
		} else {
			save("saves/" + file + ".rlvl");
		}
	}

	/**
	 * Saves current game to specified filepath.
	 * 
	 * @param fileName
	 */
	public static void save(final File file) {
		try {
			final DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
			final DocumentBuilder docBuilder = docFactory.newDocumentBuilder();

			final Document doc = docBuilder.newDocument();

			final Element gameNode = doc.createElement("game"); // <world> - ROOT
			doc.appendChild(gameNode);

			final Element dimensionsNode = doc.createElement("dimensions"); // <dimensions>
			gameNode.appendChild(dimensionsNode);

			for (final Dimension dimension : Game.getDimensions()) {

				final Element dimensionNode = doc.createElement("dimension"); // <dimension>
				dimensionsNode.appendChild(dimensionNode);

				dimensionNode.setAttribute("id", "0");
				dimensionNode.setAttribute("name", dimension.getName());

				final Element worldsNode = doc.createElement("worlds"); // <worlds>
				dimensionNode.appendChild(worldsNode);

				for (final World world : dimension.getWorlds()) {

					final Element worldNode = doc.createElement("world"); // <world>
					worldsNode.appendChild(worldNode);

					worldNode.setAttribute("id", "0");
					worldNode.setAttribute("name", world.getName());
					worldNode.setAttribute("type", world.getType().name());
					worldNode.setAttribute("time", world.getTime().getLocalTime() + "");

					final Element seasonsNode = doc.createElement("seasons"); // <seasons>
					worldNode.appendChild(seasonsNode);

					for (final WorldSeason season : world.getTime().getSeasons()) {

						final Element seasonNode = doc.createElement("season"); // <season>
						seasonsNode.appendChild(seasonNode);

						seasonNode.setAttribute("name", season.getName());
						seasonNode.setAttribute("duration", season.getDuration() + "");
						seasonNode.setAttribute("color",
								"#" + Integer.toHexString(season.getUIColor().getRGB()).substring(2));

					}

					final Element mapNode = doc.createElement("map"); // <map>
					worldNode.appendChild(mapNode);

					for (final Chunk chunk : world.getTerrain().getChunks()) {

						int x = chunk.getX();
						int y = chunk.getY();
						int chunkX = x * Chunk.SIZE;
						int chunkY = y * Chunk.SIZE;
						for (int cx = 0; cx < Chunk.SIZE; cx++) {
							for (int cy = 0; cy < Chunk.SIZE; cy++) {
								Tile tile = chunk.getLocalTileAt(cx, cy);
								if (tile == null) {
									continue;
								}
								int tileX = chunkX + cx;
								int tileY = chunkY + cy;

								final Element tileNode = doc.createElement("tile"); // <tile>
								mapNode.appendChild(tileNode);

								tileNode.setAttribute("id", Terrain.resourcePathToTileID(tile.getResourcePath()) + "");
								tileNode.setAttribute("X", tileX + "");
								tileNode.setAttribute("Y", tileY + "");
							}

						}

					}

					final Element entitiesNode = doc.createElement("entities"); // <entities>
					worldNode.appendChild(entitiesNode);

					for (final Entity entity : world.getEntities()) {

						final Element entityNode = doc.createElement("entity"); // <tile>
						entitiesNode.appendChild(entityNode);

						// entityNode.setAttribute("id", entity.getID() + "");
						entityNode.setAttribute("name", entity.getName());
						entityNode.setAttribute("type", entity.getResourcePath().toString(false));

						final Element speedNode = doc.createElement("speed"); // <speed>
						entityNode.appendChild(speedNode);
						speedNode.appendChild(doc.createTextNode(entity.getSpeed() + ""));

						final Element positionNode = doc.createElement("position"); // <position>
						entityNode.appendChild(positionNode);

						final Element xNode = doc.createElement("X"); // <X>
						final Element yNode = doc.createElement("Y"); // <Y>
						final Element zNode = doc.createElement("Z"); // <Z>
						final Element rotationNode = doc.createElement("rotation"); // <rotation>
						positionNode.appendChild(xNode);
						positionNode.appendChild(yNode);
						positionNode.appendChild(zNode);
						positionNode.appendChild(rotationNode);
						final WorldPosition position = entity.getPosition();
						xNode.appendChild(doc.createTextNode(position.getX() + ""));
						yNode.appendChild(doc.createTextNode(position.getY() + ""));
						zNode.appendChild(doc.createTextNode(position.getZ() + ""));
						rotationNode.appendChild(doc.createTextNode(position.getRotation() + ""));

						final Element inventoryNode = doc.createElement("inventory"); // <inventory>
						entityNode.appendChild(inventoryNode);
						inventoryNode.setAttribute("slots", "1");

					}

					final Element objectsNode = doc.createElement("objects"); // <objects>
					worldNode.appendChild(objectsNode);

					for (final WorldObject object : world.getObjects()) {

						final Element objectNode = doc.createElement("object"); // <object>
						objectsNode.appendChild(objectNode);

						// entityNode.setAttribute("id", entity.getID() + "");
						objectNode.setAttribute("type", object.getResourcePath().toString(false));

						final Element engagedNode = doc.createElement("engaged"); // <engaged>
						objectNode.appendChild(engagedNode);
						engagedNode.appendChild(doc.createTextNode(boolToString(false)));

						final Element positionNode = doc.createElement("position"); // <position>
						objectNode.appendChild(positionNode);

						final Element xNode = doc.createElement("X"); // <X>
						final Element yNode = doc.createElement("Y"); // <Y>
						final Element zNode = doc.createElement("Z"); // <Z>
						final Element rotationNode = doc.createElement("rotation"); // <rotation>
						positionNode.appendChild(xNode);
						positionNode.appendChild(yNode);
						positionNode.appendChild(zNode);
						positionNode.appendChild(rotationNode);
						final WorldPosition position = object.getPosition();
						xNode.appendChild(doc.createTextNode(position.getX() + ""));
						yNode.appendChild(doc.createTextNode(position.getY() + ""));
						zNode.appendChild(doc.createTextNode(position.getZ() + ""));
						rotationNode.appendChild(doc.createTextNode(position.getRotation() + ""));

					}

				}

				final Element eventsNode = doc.createElement("events"); // <events>
				dimensionNode.appendChild(eventsNode);

				for (final Event event : dimension.getEvents()) {

					final Element eventNode = doc.createElement("event"); // <event>
					eventsNode.appendChild(eventNode);

					eventNode.setAttribute("id", "0");
					eventNode.setAttribute("name", event.getName());

				}

			}

			final TransformerFactory transformerFactory = TransformerFactory.newInstance();
			final Transformer transformer = transformerFactory.newTransformer();
			final DOMSource source = new DOMSource(doc);
			final StreamResult result = new StreamResult(file);

			transformer.transform(source, result);

			System.out.println("World saved to file!");

		} catch (final ParserConfigurationException pce) {
			pce.printStackTrace();
		} catch (final TransformerException tfe) {
			tfe.printStackTrace();
		}
	}

	public static void load(final String file) {
		if (file.endsWith(".rlvl")) {
			load(new File(file));
		} else {
			load("saves/" + file + ".rlvl");
		}
	}

	public static void load(final File[] files) {
		if (files.length > 0) {
			load(files[0]);
		} else {
			System.out.println("NO FILE GIVEN TO SAVELOADER!");
		}
	}

	public static void load(final File file) {

		// Reading save file
		final DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder dBuilder = null;
		Document root = null;
		try {
			dBuilder = dbFactory.newDocumentBuilder();
			root = dBuilder.parse(file);
			root.getDocumentElement().normalize();
			Game.unload();
			final ArrayList<Node> dimensions = getTagsOf(root, "dimensions");
			for (final Node dimensionNode : dimensions) {
				final ArrayList<World> worlds = new ArrayList<World>();
				final ArrayList<Event> events = new ArrayList<Event>();
				final String dimensionName = ((Element) dimensionNode).getAttribute("name");
				final ArrayList<Node> worldNodes = getTagsOf(root, "worlds", dimensionNode.getChildNodes());
				for (final Node world : worldNodes) {
					final String worldName = ((Element) world).getAttribute("name");
					final String type = ((Element) world).getAttribute("type").toUpperCase();
					final int time = Integer.parseInt(((Element) world).getAttribute("time"));
					WorldType worldType = WorldType.OVERWORLD;
					if (type.equals("HELLISH")) {
						worldType = WorldType.HELLISH;
					} else if (type.equals("VOID")) {
						worldType = WorldType.VOID;
					}
					// Compile time
					final ArrayList<WorldSeason> seasons = new ArrayList<WorldSeason>();
					final ArrayList<Node> seasonNodes = getTagsOf(root, "seasons", world.getChildNodes());
					for (final Node season : seasonNodes) {
						final String name = ((Element) season).getAttribute("name");
						final int duration = Integer.parseInt(((Element) season).getAttribute("duration"));
						final Color color = Color.decode(((Element) season).getAttribute("color"));
						seasons.add(new WorldSeason(name, duration, color));
					}
					// Compile tiles
					final ArrayList<Tile> tiles = new ArrayList<Tile>();
					final ArrayList<Node> tileNodes = getTagsOf(root, "map", world.getChildNodes());
					for (final Node tileNode : tileNodes) {
						final long x = Long.parseLong(((Element) tileNode).getAttribute("X"));
						final long y = Long.parseLong(((Element) tileNode).getAttribute("Y"));
						final int tileID = Integer.parseInt(((Element) tileNode).getAttribute("id"));
						ResourcePath resourcePath = Terrain.tileIDToResourcePath(tileID);
						String ID = resourcePath.toString(false);
						Tile tile = Tile.createAbstractTile(ID, x, y);
						tiles.add(tile);
					}
					// Compile entities
					final ArrayList<Entity> entities = new ArrayList<Entity>();
					final ArrayList<Node> entityNodes = getTagsOf(root, "entities", world.getChildNodes());
					for (final Node entity : entityNodes) {
						final String name = ((Element) entity).getAttribute("name");
						final String entityType = ((Element) entity).getAttribute("type");
						@SuppressWarnings("unused")
						float x = 0, y = 0, z = 0, rotation = 0, speed = 0;
						final NodeList properties = entity.getChildNodes();
						for (int i = 0; i < properties.getLength(); i++) {
							final Node property = properties.item(i);
							switch (property.getNodeName()) {
							case "speed":
								speed = Float.parseFloat(property.getTextContent());
								break;
							}
						}
						final ArrayList<Node> positionalNodes = getTagsOf(root, "position", entity.getChildNodes());
						for (final Node position : positionalNodes) {
							switch (position.getNodeName()) {
							case "X":
								x = Float.parseFloat(position.getTextContent());
								break;
							case "Y":
								y = Float.parseFloat(position.getTextContent());
								break;
							case "Z":
								z = Float.parseFloat(position.getTextContent());
								break;
							case "rotation":
								rotation = Float.parseFloat(position.getTextContent());
								break;
							}
						}
						final WorldPosition position = new WorldPosition(x, y, z, rotation, worldName);
						ResourcePath resourcePath = new ResourcePath("entity", entityType);
						entities.add(Entity.create(resourcePath, position, name));
					}
					// Compile objects
					final ArrayList<WorldObject> objects = new ArrayList<WorldObject>();
					final ArrayList<Node> objectNodes = getTagsOf(root, "objects", world.getChildNodes());
					for (final Node object : objectNodes) {
						final String objectType = ((Element) object).getAttribute("type");
						float x = 0, y = 0, z = 0, rotation = 0;
						final ArrayList<Node> positionalNodes = getTagsOf(root, "position", object.getChildNodes());
						for (final Node position : positionalNodes) {
							switch (position.getNodeName()) {
							case "X":
								x = Float.parseFloat(position.getTextContent());
								break;
							case "Y":
								y = Float.parseFloat(position.getTextContent());
								break;
							case "Z":
								z = Float.parseFloat(position.getTextContent());
								break;
							case "rotation":
								rotation = Float.parseFloat(position.getTextContent());
								break;
							}
						}
						final WorldPosition position = new WorldPosition(x, y, z, rotation, worldName);
						ResourcePath resourcePath = new ResourcePath("object", objectType);
						objects.add(WorldObject.create(resourcePath, position));
					}
					final WorldSeason[] worldSeasons = seasons.toArray(new WorldSeason[seasons.size()]);
					final Terrain terrain = new Terrain(tiles.toArray(new Tile[tiles.size()]));
					worlds.add(new World(worldName, new WorldTime(time, worldSeasons), worldType, terrain, objects,
							entities));
				}
				final ArrayList<Node> eventNodes = getTagsOf(root, "events", dimensionNode.getChildNodes());
				for (final Node event : eventNodes) {
					final String eventName = ((Element) event).getAttribute("name");
					events.add(new Event(eventName));
				}
				Game.addDimension(new Dimension(dimensionName, worlds, events));
			}
			Game.setLoaded(true);
		} catch (final NumberFormatException e) {
			System.err.println("Unable to load specified savefile due to its incorrect format!");
		} catch (final Exception e) {
			return;
		}
	}

	private static ArrayList<Node> getTagsOf(final Document root, final String tag) {
		return getTagsOf(root, tag, null);
	}

	private static ArrayList<Node> getTagsOf(final Document root, final String tag, NodeList list) {
		final ArrayList<Node> tagList = new ArrayList<Node>();
		if (list == null) {
			list = root.getElementsByTagName(tag);
		}
		for (int i = 0; i < list.getLength(); i++) {
			final Node node = list.item(i);
			if (node.getNodeName().equals(tag)) {
				list = node.getChildNodes();
				break;
			}
		}
		for (int i = 0; i < list.getLength(); i++) {
			final Node otherNode = list.item(i);
			if (otherNode.getNodeType() == Node.ELEMENT_NODE) {
				tagList.add(otherNode);
			}
		}
		return tagList;
	}
}
