package core.engine;

import core.Window;

/**
 * Engine
 * 
 * @author Lawrence Bensaid <lawrencebensaid@icloud.com>
 */
public class Engine {

	public static void start() {
		Window.init();
		Cycle.start();
	}
	
}