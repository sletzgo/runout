package core.engine;

import core.Window;
import game.World;

/**
 * Cycle
 * 
 * @author Lawrence Bensaid <lawrencebensaid@icloud.com>
 */
public class Cycle {

    private static boolean running = false;

    // Configuration
    private static final int MAX_UPDATES = 5;
    private static int desiredFPS = 60;
    private static int targetTime = 1000000000 / desiredFPS;
    
    // Dynamic
    private static long lastUpdateTime = 0;
    private static int updates = 0;
    private static int currentFPS = 0;

    public static void start() {
        Thread thread = new Thread() {
            public void run() {
                running = true;

                lastUpdateTime = System.nanoTime();

                int fps = 0;
                long lastFpsCheck = System.nanoTime();

                while(running) {
                    long currentTime = System.nanoTime();

                    updates = 0;

                    while(currentTime - lastUpdateTime >= targetTime) {
                        World world = net.Client.getLoadedWorld();
                        if (world != null) {
                            world.onUpdate();
                        }
                        Window.camera.onUpdate();
                        lastUpdateTime += targetTime;
                        updates += 1;

                        if (updates > MAX_UPDATES) {
                            break;
                        }
                    }

                    Window.render();

                    fps++;
                    if(System.nanoTime() >= lastFpsCheck + 1000000000) {
                        currentFPS = fps;
                        fps = 0;
                        lastFpsCheck = System.nanoTime();
                    }

                    long timeTaken = System.nanoTime() - currentTime;
                    if (targetTime > timeTaken) {
                        try {
                            Thread.sleep((targetTime - timeTaken) / 1000000);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        };
        thread.setName("Gameloop");
        thread.start();
    }

    public static float updateDelta() {
        return 1.0f / 1000000000 * targetTime;
    }

    public static int getFPS() {
        return currentFPS;
    }

}