package core;

import com.jogamp.opengl.GL2;
import com.jogamp.opengl.GLAutoDrawable;
import com.jogamp.opengl.GLEventListener;

import game.Game;
import game.World;

/**
 * @author Lawrence Bensaid <lawrencebensaid@icloud.com>
 */
public class Renderer implements GLEventListener {

	public static GL2 gl = null;
	private int i = 0;

	@Override
	public void display(GLAutoDrawable drawable) {
		gl = drawable.getGL().getGL2();
		gl.glClear(GL2.GL_COLOR_BUFFER_BIT);

		gl.glTranslatef(-Window.camera.getX(), -Window.camera.getY(), 0);
		World world = Game.getWorld("Overworld");
		if (world != null) {
			world.onRender();
			System.out.println(i);
			i += 0.1;
		}
		gl.glTranslatef(Window.camera.getX(), Window.camera.getY(), 0);
	}

	@Override
	public void dispose(GLAutoDrawable drawable) {
	}

	@Override
	public void init(GLAutoDrawable drawable) {
		GL2 gl = drawable.getGL().getGL2();
		gl.glClearColor(0, 0, 0, 1);
		
		gl.glEnable(GL2.GL_TEXTURE_2D);
	}

	@Override
	public void reshape(GLAutoDrawable drawable, int arg1, int arg2, int arg3, int arg4) {
		GL2 gl = drawable.getGL().getGL2();

		gl.glMatrixMode(GL2.GL_PROJECTION);
		gl.glLoadIdentity();
		float unitsTall = Window.getHeight() / (Window.getWidth() / Window.camera.getViewport());
		gl.glOrthof(-Window.camera.getViewport() / 2, Window.camera.getViewport() / 2, -unitsTall / 2, unitsTall / 2, -1, 1);
		gl.glMatrixMode(GL2.GL_MODELVIEW);
	}

}
