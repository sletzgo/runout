package game;

import static util.RadiansUtil.directionToHorizontalVelocity;
import static util.RadiansUtil.directionToVerticalVelocity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

/**
 * An entity is anything that has behaviour and can move.
 * 
 * @since 0.1.0
 * @author Lawrence Bensaid <lawrencebensaid@icloud.com>
 */
@SuppressWarnings("unused")
public class Entity extends GameObject implements EntityProtocol {

	private final static ArrayList<Entity> ENTITIES = new ArrayList<Entity>();
	private static boolean resourcesLoaded = false;

	public static boolean resourcesLoaded() {
		return resourcesLoaded;
	}

	public static void loadResourcesIfNeeded() {
		if (!resourcesLoaded) {
			ENTITIES.add(new game.entity.Player());
			ENTITIES.add(new game.entity.Assailant());
			ENTITIES.add(new game.entity.Companion());
			ENTITIES.add(new game.entity.Item());
			resourcesLoaded = true;
		}
	}

	public static Entity[] types() {
		return ENTITIES.toArray(new Entity[ENTITIES.size()]);
	}

	public static Entity create(ResourcePath resourcePath, WorldPosition position, String name) {
		// TODO: Create from resourcepath
		switch (resourcePath.toString(false)) {
		case "Player":
			return new game.entity.Player(position, name);
		case "Assailant":
			return new game.entity.Assailant(position, name);
		case "Companion":
			return new game.entity.Companion(position, name);
		case "Item":
			return new game.entity.Item(position, name);
		default:
			return null;
		}
	}

	private final ResourcePath resourcePath;
	private int health = 0;
	private boolean destroyed = false;
	private boolean isInvisible = false;
	private boolean isCollectable = false;
	private String name;
	private String sprite = "entity/default.png";

	/**
	 * Abstract
	 * 
	 * @param TYPE
	 */
	protected Entity(String TYPE) {
		super(new WorldPosition());
		resourcePath = new ResourcePath("entity", TYPE);
	}

	public Entity(String TYPE, WorldPosition position) {
		this(TYPE, position, "Entity");
	}

	public Entity(String TYPE, GameObject object) {
		this(TYPE, object, "Entity");
	}

	public Entity(String TYPE, WorldPosition position, String name) {
		this(TYPE, new GameObject(position), name);
	}

	public Entity(String TYPE, GameObject object, String name) {
		super(object.position);
		this.name = name;
		this.position = object.position;
		this.width = object.width;
		this.height = object.height;
		this.speed = object.speed;
		this.speedMax = object.speedMax;
		resourcePath = new ResourcePath("entity", TYPE);
		onCreate();
		System.out.println("Entity " + TYPE + " '" + name + "' has spawned!");
	}

	/**
	 * @return the name of the Entity if it has one.
	 */
	public String getName() {
		return name;
	}

	/**
	 * The story of this entity will come to an end.
	 * 
	 * @deprecated Entity instances are managed by the {@link game.World}.
	 */
	public void destroy() {
		destroyed = true;
		onDestroy();
	}

	/**
	 * Checks if the entity is still in existance.
	 * 
	 * @deprecated Entity instances are managed by the {@link game.World}.
	 */
	public boolean isDestroyed() {
		return destroyed;
	}

	/**
	 * Result format example: {@code entity.Player} or {@code entity.Assailant}.
	 * 
	 * @return type as a ResourcePath.
	 */
	public ResourcePath getResourcePath() {
		return resourcePath;
	}

	@Override
	public String toString() {
		return resourcePath.toString();
	}

	@Override
	public void onCreate() {
	}

	@Override
	public void onUpdate() {

		boolean canMoveUp = true;
		boolean canMoveRight = true;
		boolean canMoveDown = true;
		boolean canMoveLeft = true;
		if (canMoveLeft && position.direction > 90 && position.direction < 270) {
			position.x += (float) directionToHorizontalVelocity(position.direction, speed);
		}
		if (canMoveRight && ((position.direction < 90 && position.direction >= 0)
				|| (position.direction > 270 && position.direction < 360))) {
			position.x += (float) directionToHorizontalVelocity(position.direction, speed);
		}
		if (canMoveDown && position.direction > 0 && position.direction < 180) {
			position.y += (float) directionToVerticalVelocity(position.direction, speed);
		}
		if (canMoveUp && position.direction > 180 && position.direction < 360) {
			position.y += (float) directionToVerticalVelocity(position.direction, speed);
		}
	}

	@Override
	public void onDestroy() {
	}

}
