package game;

import static util.RadiansUtil.getAngle;

/**
 * WorldPosition
 * 
 * @since 0.1.0
 * @author Lawrence Bensaid <lawrencebensaid@icloud.com>
 */
public class WorldPosition {

    public float x;
    public float y;
    public float z;
	public float direction;
    private String worldName;

    public WorldPosition() {
        this(0, 0);
    }

    public WorldPosition(float xPos, float yPos) {
        this(xPos, yPos, 0);
    }
    public WorldPosition(float xPos, float yPos, float rotation) {
        this(xPos, yPos, 0, rotation);
    }

    public WorldPosition(float xPos, float yPos, float zPos, float rotation) {
        this(xPos, yPos, 0, rotation, "Overworld");
    }

    public WorldPosition(float xPos, float yPos, String worldName) {
        this(xPos, yPos, 0, 0, worldName);
    }

    public WorldPosition(float xPos, float yPos, float rotation, String worldName) {
        this(xPos, yPos, 0, rotation, worldName);
    }

    public WorldPosition(float xPos, float yPos, float zPos, float rotation, String worldName) {
        this.x = xPos;
        this.y = yPos;
        this.z = zPos;
        this.direction = rotation;
        this.worldName = worldName;
    }

    @Override
    public String toString() {
        return "DIR: " + direction + "; X: " + x + "; Y: " + y + ";";
    }

    public String toString(boolean verbose) {
        if (verbose) {
            return "WORLD: " + worldName + "; DIR: " + direction + "; X: " + x + "; Y: " + y + "Z: " + z + ";";
        }
        return toString();
    }

    public void setX(float x) {
        this.x = x;
    }

    public void setY(float y) {
        this.y = y;
    }

    public void setZ(float z) {
        this.z = z;
    }

    public float getX() {
        return x;
    }

    public float getY() {
        return y;
    }

    public float getZ() {
        return z;
    }

    public float getRotation() {
        return direction;
    }

    public void setRotation(float rotation) {
        direction = rotation;
    }

    public String getWorldName() {
        return worldName;
    }

    public World getWorld() {
        return Game.getWorld(worldName);
    }

	/**
	 * Returns the distance from WorldPosition to other WorldPosition.
	 * 
	 * @param fromPos
	 * @param toPos
	 * @return Double
	 */
	public static float getDistance(WorldPosition fromPos, WorldPosition toPos) {
		return (float) Math.hypot(fromPos.getX() - toPos.getX(), fromPos.getY() - toPos.getY());
	}

	/**
	 * Returns the angle from WorldPosition to other WorldPosition
	 * 
	 * @param fromPos
	 * @param toPos
	 * @return Double
	 */
	public static float getDirection(WorldPosition fromPos, WorldPosition toPos) {
		return getAngle(toPos.getX() - fromPos.getX(), toPos.getY() - fromPos.getY());
	}
    
}