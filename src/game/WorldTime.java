package game;

import java.awt.Color;

/**
 * WorldTime
 * 
 * @since 0.1.0
 * @author Lawrence Bensaid <lawrencebensaid@icloud.com>
 */
public class WorldTime {

    private final int MINUTES_PER_HOUR = 1;
    private final int HOURS_PER_DAY = 1;
    private final int localOffset;
    private final WorldSeason[] seasons;

    public WorldTime() {
        this(0);
    }

    public WorldTime(int localOffset) {
        this(localOffset, new WorldSeason[] {
            new WorldSeason("Spring", 10, Color.yellow),
            new WorldSeason("Summer", 20, Color.green),
            new WorldSeason("Autum", 10, Color.orange),
            new WorldSeason("Winter", 10, Color.cyan)
        });
    }

    public WorldTime(int localOffset, WorldSeason[] seasons) {
        this.localOffset = localOffset;
        this.seasons = seasons;
    }

    /**
     * Offset from game time.
     * 
     * @return Local relative time.
     */
	public int getLocalTime() {
		return localOffset;
	}

    /**
     * How many days a year has. This value generally doesn't change.
     * 
     * @return Days per year.
     */
    public int getDaysPerYear() {
        int days = 0;
        for (WorldSeason season : seasons) {
            days += season.getDuration();
        }
        return days;
    }

    /**
     * @return Seasons of this time.
     */
    public WorldSeason[] getSeasons() {
        return seasons;
    }

    /**
     * Calculates a string 
     * 
     * @return 
     */
    public String getClock() {
        return String.format("%02d", getMinuteOfHour()) + ":" + String.format("%02d", getHourOfDay());
    }

    /**
     * @return Step.
     */
    public int getStep() {
        return Game.getTime().getStep() - localOffset;
    }

    /**
     * @return the time measured in seconds.
     */
    public int getSecond() {
        return getStep();
    }

    /**
     * @return the time measured in minutes.
     */
    public int getMinute() {
        return getSecond() / MINUTES_PER_HOUR;
    }

    /**
     * @return the minute of the current hour.
     */
    public int getMinuteOfHour() {
        return getSecond() % MINUTES_PER_HOUR;
    }

    /**
     * @return the time measured in hours.
     */
    public int getHour() {
        return getMinute() / HOURS_PER_DAY;
    }

    /**
     * @return the hour of the current day.
     */
    public int getHourOfDay() {
        return getMinute() % HOURS_PER_DAY;
    }

    /**
     * @return the time measured in days.
     */
    public int getDay() {
        return getStep() / MINUTES_PER_HOUR / HOURS_PER_DAY;
    }

    /**
     * 
     * @return Day of year.
     */
    public int getDayOfYear() {
        return getDay() % getDaysPerYear();
    }

    /**
     * @return Current season.
     */
    public WorldSeason getSeason() {
        int start = 0;
        for (WorldSeason season : seasons) {
            if ((season.getDuration() + start) >= getDayOfYear()) {
                return season;
            }
            start += season.getDuration();
        }
        if (seasons.length > 0) {
            return seasons[0];
        }
        return null;
    }

    /**
     * 
     * @return Current year.
     */
    public int getYear() {
        return getDay() / getDaysPerYear();
    }

}