package game;

/**
 * EntityProtocol
 * 
 * @author Lawrence Bensaid <lawrencebensaid@icloud.com>
 */
public interface EntityProtocol {

    public void onCreate();

    public void onUpdate();

    public void onRender();

    public void onDestroy();
    
}