package game.entity;

import game.Entity;
import game.GameObject;
import game.WorldPosition;

public class Companion extends Entity {

	public Companion() {
		super("Companion");
	}

    public Companion(WorldPosition position, String name) {
        super("Companion", position, name);
    }

	public Companion(GameObject object, String name) {
		super("Companion", object, name);
	}

}
