package game.entity;

import game.Entity;
import game.WorldPosition;

/**
 * Item
 */
public class Item extends Entity {

    @SuppressWarnings("unused")
    private game.Item item;

	public Item() {
		super("Item");
	}

    public Item(WorldPosition position, String name) {
        this(position, name, null);
    }

    public Item(WorldPosition position, String name, game.Item item) {
        super("Item", position, name);
        this.item = item;
    }

}