package game.entity;

import core.Graphics;
import core.Window;
import core.input.KeyInput;
import game.Entity;
import game.GameObject;
import game.WorldPosition;
// import net.Client;
import util.ImageResource;

public class Player extends Entity {

	public static ImageResource image = new ImageResource("entity/player.jpg");
	public boolean focus = true;

	public Player() {
		super("Player");
	}

	public Player(WorldPosition position, String name) {
		super("Player", position, name);
	}

	public Player(GameObject object, String name) {
		super("Player", object, name);
	}

	public boolean isConnected() {
		return false;
	}

	@Override
	public void onCreate() {

	}

	@Override
	public void onUpdate() {

		if (KeyInput.getKey(KeyInput.RIGHT)) {
			setRotation(0);
			setSpeed(speedMax);
		} else if (KeyInput.getKey(KeyInput.RIGHT) && KeyInput.getKey(KeyInput.DOWN)) {
			setRotation(45);
			setSpeed(speedMax);
		} else if (KeyInput.getKey(KeyInput.DOWN)) {
			setRotation(270);
			setSpeed(speedMax);
		} else if (KeyInput.getKey(KeyInput.DOWN) && KeyInput.getKey(KeyInput.LEFT)) {
			setRotation(225);
			setSpeed(speedMax);
		} else if (KeyInput.getKey(KeyInput.LEFT)) {
			setRotation(180);
			setSpeed(speedMax);
		} else if (KeyInput.getKey(KeyInput.LEFT) && KeyInput.getKey(KeyInput.UP)) {
			setRotation(315);
			setSpeed(speedMax);
		} else if (KeyInput.getKey(KeyInput.UP)) {
			setRotation(90);
			setSpeed(speedMax);
		} else if (KeyInput.getKey(KeyInput.UP) && KeyInput.getKey(KeyInput.RIGHT)) {
			setRotation(135);
			setSpeed(speedMax);
		} else {
			setSpeed(0);
		}

		if (focus) { // Can be disabled if the camera is needed for a scene.
			Window.camera.goTo(position);
		}

		super.onUpdate();
	}

	@Override
	public void setSpeed(float speed) {
		// net.Client.out("speed: " + speed + ";");
		super.setSpeed(speed);
	}

	@Override
	public void setRotation(float rotation) {
		// net.Client.out("rotation: " + rotation + ";");
		super.setRotation(rotation);
	}

	@Override
	public void onRender() {
		Graphics.drawImage(image, position.x, position.y, getWidth(), getHeight());
	}

	@Override
	public void onDestroy() {

	}

}
