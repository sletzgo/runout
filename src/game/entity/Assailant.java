package game.entity;

import core.Graphics;
import game.Entity;
import game.GameObject;
import game.WorldPosition;
import util.ImageResource;

public class Assailant extends Entity {
    
	public static ImageResource image = new ImageResource("terrain/grass.jpg");

	public Assailant() {
		super("Assailant");
	}

    public Assailant(WorldPosition position, String name) {
        super("Assailant", position, name);
    }

	public Assailant(GameObject object, String name) {
		super("Assailant", object, name);
	}

	@Override
	public void onRender() {
        Graphics.setColor(1, 0, 0, 1);
		Graphics.fillRect(position.getX(), position.getY(), getWidth(), getHeight());
	}

}
