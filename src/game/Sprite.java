package game;

import static util.ImageResource.toBufferedImage;

import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.imageio.ImageIO;

import util.ImageResource;

/**
 * Used to load the Sprite into the world.
 * 
 * @see game.Tile
 * @since 0.1.0
 * @author Lawrence Bensaid <lawrencebensaid@icloud.com>
 */
public class Sprite {

	public static final int WIDTH = 256;
	public static final int HEIGHT = 256;
	private static final Map<String, Image> sprites = new HashMap<String, Image>();
	private static boolean resourcesLoaded = false;

	public static boolean resourcesLoaded() {
		return resourcesLoaded;
	}

	public static void loadResourcesIfNeeded() {
		if (resourcesLoaded) {
			return;
		}

		try {
			Toolkit toolkit = Toolkit.getDefaultToolkit();
			BufferedImage tileset = ImageIO.read(new File("resources/terrain/world.png"));
			Image water = toolkit.getImage("resources/terrain/water.gif");
			Image waterStatic = toolkit.getImage("resources/terrain/water_static.png");
			Image gateDisengaged = toolkit.getImage("resources/object/stargate_disengaged.png");
			Image gateDisengage = toolkit.getImage("resources/object/stargate_disengage.gif");
			Image gateEngaged = toolkit.getImage("resources/object/stargate_engaged.gif");
			Image gateEngage = toolkit.getImage("resources/object/stargate_engage.gif");

			// Water
			sprites.put("tile.water", water);
			sprites.put("tile.water-still", waterStatic);

			// Grass
			sprites.put("tile.grass.top-left", getSubimage(tileset, 16, 41, 6));
			sprites.put("tile.grass.top", getSubimage(tileset, 16, 42, 6));
			sprites.put("tile.grass.top-right", getSubimage(tileset, 16, 43, 6));
			sprites.put("tile.grass.left", getSubimage(tileset, 16, 41, 7));
			sprites.put("tile.grass", getSubimage(tileset, 16, 42, 7));
			sprites.put("tile.grass.right", getSubimage(tileset, 16, 43, 7));
			sprites.put("tile.grass.bottom-left", getSubimage(tileset, 16, 41, 8));
			sprites.put("tile.grass.bottom-right", getSubimage(tileset, 16, 43, 8));
			sprites.put("tile.grass.cliff-left", getSubimage(tileset, 16, 41, 9));
			sprites.put("tile.grass.cliff", getSubimage(tileset, 16, 42, 9));
			sprites.put("tile.grass.cliff-right", getSubimage(tileset, 16, 43, 9));

			// Grass 2
			sprites.put("tile.grass2.top-left", getSubimage(tileset, 16, 41 + 4, 6));
			sprites.put("tile.grass2.top", getSubimage(tileset, 16, 42 + 4, 6));
			sprites.put("tile.grass2.top-right", getSubimage(tileset, 16, 43 + 4, 6));
			sprites.put("tile.grass2.left", getSubimage(tileset, 16, 41 + 4, 7));
			sprites.put("tile.grass2", getSubimage(tileset, 16, 42 + 4, 7));
			sprites.put("tile.grass2.right", getSubimage(tileset, 16, 43 + 4, 7));
			sprites.put("tile.grass2.bottom-left", getSubimage(tileset, 16, 41 + 4, 8));
			sprites.put("tile.grass2.bottom-right", getSubimage(tileset, 16, 43 + 4, 8));
			sprites.put("tile.grass2.cliff-left", getSubimage(tileset, 16, 41 + 4, 9));
			sprites.put("tile.grass2.cliff", getSubimage(tileset, 16, 42 + 4, 9));
			sprites.put("tile.grass2.cliff-right", getSubimage(tileset, 16, 43 + 4, 9));

			// Paving
			sprites.put("tile.stone", getSubimage(tileset, 16, 37, 3));
			sprites.put("tile.sand", getSubimage(tileset, 16, 1, 1));

			// Trees
			sprites.put("object.tree.birch-summer", getSubimage(tileset, 16, 3, 5, 3, 4));
			sprites.put("object.tree.birch-summer-snow", getSubimage(tileset, 16, 3, 17, 3, 4));
			sprites.put("object.tree.birch-summer-snow-heavy", getSubimage(tileset, 16, 3, 29, 3, 4));
			sprites.put("object.tree.oak-summer", getSubimage(tileset, 16, 0, 5, 3, 4));
			sprites.put("object.tree.oak-summer-snow", getSubimage(tileset, 16, 0, 17, 3, 4));
			sprites.put("object.tree.oak-summer-snow-heavy", getSubimage(tileset, 16, 0, 29, 3, 4));
			sprites.put("object.tree.oak-autum", getSubimage(tileset, 16, 12, 13, 3, 4));
			sprites.put("object.tree.oak-spring", getSubimage(tileset, 16, 15, 13, 3, 4));
			sprites.put("object.tree.pine-summer", getSubimage(tileset, 16, 6, 9, 3, 4));
			sprites.put("object.tree.pine-summer-snow", getSubimage(tileset, 16, 6, 17, 3, 4));
			sprites.put("object.tree.pine-summer-snow-heavy", getSubimage(tileset, 16, 6, 13, 3, 4));
			sprites.put("object.tree.pine-winter", getSubimage(tileset, 16, 9, 9, 3, 4));
			sprites.put("object.tree.pine-winter-snow", getSubimage(tileset, 16, 9, 17, 3, 4));
			sprites.put("object.tree.pine-winter-snow-heavy", getSubimage(tileset, 16, 9, 13, 3, 4));
			sprites.put("object.tree.pine-spring", getSubimage(tileset, 16, 15, 9, 3, 4));
			sprites.put("object.tree.pine-autum", getSubimage(tileset, 16, 12, 9, 3, 4));
			sprites.put("object.tree.bigpine-summer", getSubimage(tileset, 16, 10, 21, 3, 5));
			sprites.put("object.tree.bigpine-winter", getSubimage(tileset, 16, 13, 21, 3, 5));
			sprites.put("object.tree.bigpine-frozen", getSubimage(tileset, 16, 16, 21, 3, 5));
			sprites.put("object.tree.bigpine-autum", getSubimage(tileset, 16, 10, 36, 3, 5));
			sprites.put("object.tree.bigpine-spring", getSubimage(tileset, 16, 13, 36, 3, 5));

			// Objects
			sprites.put("object.sign", getSubimage(tileset, 16, 42, 27, 1, 3));
			sprites.put("object.lantern.single", getSubimage(tileset, 16, 27, 26, 1, 4));
			sprites.put("object.lantern.y", getSubimage(tileset, 16, 31, 26, 1, 4));
			sprites.put("object.lantern.x", getSubimage(tileset, 16, 28, 26, 3, 4));
			sprites.put("object.stargate", gateDisengaged);
			sprites.put("object.stargate.disengage", gateDisengage);
			sprites.put("object.stargate.engaged", gateEngaged);
			sprites.put("object.stargate.engage", gateEngage);

			resourcesLoaded = true;

		} catch (IOException e) {
			System.out.println("SPRITE RESOURCE LOADER: Failed to load terrain images: " + e.getMessage());
		}
	}

	public static Image getSubimage(BufferedImage image, int grid, int x, int y) {
		return getSubimage(image, grid, x, y, 1, 1);
	}

	public static Image getSubimage(BufferedImage image, int grid, int x, int y, int width, int height) {
		return image.getSubimage(grid * x, grid * y, grid * width, grid * height);
	}

	public static BufferedImage setUnderlay(String under, String over) {
		BufferedImage image = new BufferedImage(WIDTH, WIDTH, BufferedImage.TYPE_INT_ARGB_PRE);
		Graphics2D g2 = image.createGraphics();
		g2.drawImage(sprites.get(under), 0, 0, WIDTH, HEIGHT, null);
		g2.drawImage(sprites.get(over), 0, 0, WIDTH, HEIGHT, null);
		g2.dispose();
		return image;
	}

	private String key;
	private String underlayKey;

	public Sprite(String key) {
		this(key, null);
	}

	public Sprite(String key, String underlayKey) {
		this.key = key;
		this.underlayKey = underlayKey;
	}

	public BufferedImage getImage() {
		if (underlayKey == null) {
			return toBufferedImage(sprites.get(key));
		}
		String specialKey = key + "|" + underlayKey;
		if (sprites.get(specialKey) == null) {
			sprites.put(specialKey, setUnderlay(underlayKey, key));
		}
		BufferedImage image = toBufferedImage(sprites.get(specialKey));
		return image;
	}

	public ImageResource getImageResource() {
		return new ImageResource(sprites.get(key));
	}

	public String getKey() {
		return key;
	}

	@SuppressWarnings("unused")
	private BufferedImage getMask(final int color) {
		BufferedImage image = toBufferedImage(sprites.get(key));
		final int width = image.getWidth();
		int[] imgData = new int[width];
		for (int y = 0; y < image.getHeight(); y++) {
			image.getRGB(0, y, width, 1, imgData, 0, 1);
			for (int x = 0; x < width; x++) {
				if (imgData[x] != 0x00FFFFFF) {
					imgData[x] = color;
				}
			}
			image.setRGB(0, y, width, 1, imgData, 0, 1);
		}
		return image;
	}

}
