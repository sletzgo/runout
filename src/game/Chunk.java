package game;

import java.util.ArrayList;

/**
 * Chunk section of the terrain.
 * 
 * @see game.Tile
 * @see game.Terrain
 * @since 0.1.0
 * @author Lawrence Bensaid <lawrencebensaid@icloud.com>
 */
public class Chunk {

    public final static byte SIZE = 8;

    /**
     * @param cord Global tile coördinate.
     * @return Chunk coördinate (NOT tile coördinate).
     */
    public static int getChunkCordAt(long gt) {
        return gt == 0 ? 0 : (int) Math.floorDiv(gt, SIZE);
    }

    /**
     * @param cord Global tile coördinate.
     * @return Local tile coördinate.
     */
    public static int getLocalCordAt(long cord) {
        return (int) (cord < 0 ? Math.floorMod(cord, SIZE) : (cord % SIZE));
    }

    /**
     * @param cord Local tile X coördinate.
     * @return Global tile X coördinate.
     */
    public int getGlobalXCordAt(int cord) {
        return X == 0 ? cord : cord * X;
    }

    /**
     * @param cord Local tile Y coördinate.
     * @return Global tile Y coördinate.
     */
    public int getGlobalYCordAt(int cord) {
        return Y == 0 ? cord : cord * Y;
    }

    private final int X;
    private final int Y;
    private int[][] matrix = new int[SIZE][SIZE];

    public Chunk(int cx, int cy) {
        this(cx, cy, new ResourcePath("tile", "Water"));
    }

    public Chunk(int cx, int cy, ResourcePath resourcePath) {
        this.X = cx;
        this.Y = cy;

        for (int x = 0; x < matrix.length; x++) {
            int[] row = matrix[x];
            for (int y = 0; y < row.length; y++) {
                int ID = Terrain.resourcePathToTileID(resourcePath);
                matrix[x][y] = ID;
            }
        }

        System.out.println("Chunk created at " + X + " " + Y);
    }

    public int getX() {
        return X;
    }

    public int getY() {
        return Y;
    }

    /**
     * @return all tiles in the chunk. The amount of tiles depends on the
     *         {@link Chunk#SIZE}.
     */
    public Tile[] getTiles() {
        ArrayList<Tile> tiles = new ArrayList<Tile>();
        for (int x = 0; x < matrix.length; x++) {
            for (int y = 0; y < matrix[x].length; y++) {
                ResourcePath ID = Terrain.tileIDToResourcePath(matrix[x][y]);
                Tile tile = Terrain.getTile(ID, x, y);
                tiles.add(tile);
            }
        }
        return tiles.toArray(new Tile[tiles.size()]);
    }

    /**
     * @param ltx X coördinate in the scope of the {@link Chunk} (local coördinate).
     * @param lty Y coördinate in the scope of the {@link Chunk} (local coördinate).
     * @return tile.
     */
    public Tile getLocalTileAt(int ltx, int lty) {
        if (ltx >= 0 && lty >= 0 && ltx < matrix.length && lty < matrix[0].length) {
            int ID = matrix[ltx][lty];
            return Terrain.getTile(Terrain.tileIDToResourcePath(ID), ltx, lty);
        }
        return null;
    }

    /**
     * @param ID  Tile identifier.
     * @param ltx X coördinate in the scope of the {@link Chunk} (local coördinate).
     * @param lty Y coördinate in the scope of the {@link Chunk} (local coördinate).
     */
    public void setLocalTileAt(ResourcePath ID, int ltx, int lty) {
        if (ltx >= 0 && lty >= 0 && ltx < matrix.length && lty < matrix[0].length) {
            matrix[ltx][lty] = Terrain.resourcePathToTileID(ID);
        }
    }

}