package game;

import java.util.ArrayList;

import game.object.*;

/**
 * Interactable object in a world.
 * 
 * @since 0.1.0
 * @author Lawrence Bensaid <lawrencebensaid@icloud.com>
 */
public class WorldObject extends GameObject {

	private final static ArrayList<WorldObject> OBJECTS = new ArrayList<WorldObject>();
	private static boolean resourcesLoaded = false;

	public static boolean resourcesLoaded() {
		return resourcesLoaded;
	}

	public static void loadResourcesIfNeeded() {
		if (!resourcesLoaded) {
			OBJECTS.add(new game.object.Stargate(null));
			OBJECTS.add(new game.object.Sign(null));
			OBJECTS.add(new game.object.Lantern(null));
			OBJECTS.add(new game.object.LanternSingle(null));
			OBJECTS.add(new game.object.OakTree(null));
			OBJECTS.add(new game.object.BirchTree(null));
			OBJECTS.add(new game.object.PineTree(null));
			OBJECTS.add(new game.object.BigPineTree(null));
			resourcesLoaded = true;
		}
	}

	public static WorldObject[] types() {
		return OBJECTS.toArray(new WorldObject[OBJECTS.size()]);
	}

	/**
	 * @param resourcePath
	 * @param position
	 * @return
	 */
	public static WorldObject create(ResourcePath resourcePath, WorldPosition position) {
		// TODO: Create from resourcepath
		switch (resourcePath.toString(false)) {
		case "Stargate":
			return new Stargate(position);
		case "Sign":
			return new Sign(position);
		case "Lantern":
			return new Lantern(position);
		case "LanternSingle":
			return new LanternSingle(position);
		case "OakTree":
			return new OakTree(position);
		case "BirchTree":
			return new BirchTree(position);
		case "PineTree":
			return new PineTree(position);
		case "BigPineTree":
			return new BigPineTree(position);
		default:
			return null;
		}
	}

	private final ResourcePath resourcePath;
	protected Sprite sprite;
	private boolean selected = false;

	/**
	 * Abstract
	 * 
	 * @param TYPE
	 */
	private WorldObject(String TYPE) {
		super(new WorldPosition());
		resourcePath = new ResourcePath("object", TYPE);
	}

	public WorldObject(String TYPE, Sprite sprite, WorldPosition position) {
		this(TYPE, sprite, new GameObject(position));
	}

	public WorldObject(String TYPE, Sprite sprite, GameObject object) {
		super(object.position);
		this.width = object.width;
		this.height = object.height;
		this.sprite = sprite;
		resourcePath = new ResourcePath("object", TYPE);
	}

	public Sprite getSprite() {
		return sprite;
	}

	public void setSelected() {
		selected = !selected;
	}

	public void setSelected(boolean state) {
		selected = false;
	}

	public boolean getSelected() {
		return selected;
	}

	/**
	 * Result format example: {@code object.Tree} or {@code object.Door}.
	 * 
	 * @return type as a ResourcePath.
	 */
	public ResourcePath getResourcePath() {
		return resourcePath;
	}

	@Override
	public String toString() {
		return resourcePath.toString();
	}

}
