package game;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import net.Server;
import game.object.Stargate;
import gui.ServerGUI;
import util.Version;

/**
 * Game
 * 
 * @see game.World
 * @see game.Dimension
 * @version 0.1.0
 * @since 0.1.0
 * @author Lawrence Bensaid <lawrencebensaid@icloud.com>
 */
public class Game {

    // Version
    public final static Version version = new Version(0, 1, 0);

    private static ArrayList<Dimension> dimensions = new ArrayList<Dimension>();
    private static boolean loaded = false;
    private static boolean initialised = false;
    private static int speed = 1;
    private static boolean running = false;
    private static int tileSize = 1;

    // Configuration
    private static final int MAX_UPDATES = 5;
    private static int desiredSIM = 100;
    private static int targetTime = 1000000000 / desiredSIM;

    // Dynamic
    private static long lastUpdateTime = 0;
    private static int updates = 0;
    private static int simulationSpeed = 0;
    private static Thread cycle;
    private static GameTime time = new GameTime();
    private static int miliTicks = 0;

    // Game settings
    public static boolean fogOfWar = false;

    // public static String getVersionString() {
    // return getVersionString(false);
    // }

    // public static String getVersionString(boolean verbose) {
    // String label = null;
    // String[] version = new String[] { VERSION_MAJOR + "", VERSION_MINOR + "",
    // VERSION_PATCH + "" };
    // return (verbose ? "V " : "") + String.join(".", version) + (!verbose && label
    // == null ? "" : "(" + label + ")");
    // }

    public static void initialiseIfNeeded() {
        if (!initialised) {
            Sprite.loadResourcesIfNeeded();
            Terrain.loadResourcesIfNeeded();
            Tile.loadResourcesIfNeeded();
            Entity.loadResourcesIfNeeded();
            WorldObject.loadResourcesIfNeeded();
            initialised = true;
        }
    }

    public static void setLoaded(boolean loaded) {
        Game.loaded = loaded;
    }

    public static void unload() {
        stop();
        if (ServerGUI.instance != null) {
            ServerGUI.instance.tabView.minimap.unload();
        }
        time.reset();
        dimensions = new ArrayList<Dimension>();
        loaded = false;
    }

    public static boolean isLoaded() {
        return loaded;
    }

    public static void addDimension(Dimension dimension) {
        dimensions.add(dimension);
    }

    public static Dimension[] getDimensions() {
        return Game.dimensions.toArray(new Dimension[Game.dimensions.size()]);
    }

    public static Dimension getDimension(String name) {
        for (Dimension dimension : dimensions) {
            if (dimension.getName().equals(name)) {
                return dimension;
            }
        }
        return null;
    }

    public static void addWorld(String dimensionName, World world) {
        for (int i = 0; i < dimensions.size(); i++) {
            if (dimensions.get(i).getName().equals(dimensionName)) {
                dimensions.get(i).addWorld(world);
                return;
            }
        }
    }

    public static void spawn(Entity entity) {
        for (Dimension dimension : dimensions) {
            String worldName = entity.position.getWorldName();
            dimension.getWorld(worldName).spawn(entity);
            net.Server.broadcast("entitySpawn: " + entity.getName() + "; entityX: " + entity.getX() + "; entityY: "
                    + entity.getY() + "; entityType: " + entity.toString());
            return;
        }
    }

    public static Stargate getRandomGate() {
        // TODO: Actually make this random
        World world = getWorld("Overworld");
        for (WorldObject object : world.getObjects()) {
            if (object.getResourcePath().equals("object.Stargate")) {
                return (Stargate) object;
            }
        }
        return null;
    }

    public static void setFog(boolean state) {
        if (state != fogOfWar) {
            fogOfWar = state;
            Server.broadcast("setting.fog: " + (getFog() ? "true" : "false"));
        }
    }

    public static boolean getFog() {
        return fogOfWar;
    }

    public static int getSpeed() {
        return speed;
    }

    public static int getSIM() {
        return simulationSpeed;
    }

    public static GameTime getTime() {
        return time;
    }

    public static void onUpdate() {
        miliTicks++;
        if (miliTicks >= 100) {
            time.tick();
            miliTicks = 0;
        }
        for (Dimension dimension : dimensions) {
            dimension.onUpdate();
        }
    }

    public static void stop() {
        net.Server.print("Game shutting down");
        running = false;
    }

    public static void start() {
        net.Server.print("Game started");
        Sprite.loadResourcesIfNeeded();
        cycle = new Thread() {
            public void run() {
                running = true;

                lastUpdateTime = System.nanoTime();

                int sim = 0;
                long lastFpsCheck = System.nanoTime();

                while (running) {
                    long currentTime = System.nanoTime();

                    updates = 0;

                    while (currentTime - lastUpdateTime >= targetTime) {
                        Game.onUpdate();
                        lastUpdateTime += targetTime;
                        updates += 1;

                        if (updates > MAX_UPDATES) {
                            break;
                        }
                    }

                    sim++;
                    if (System.nanoTime() >= lastFpsCheck + 1000000000) {
                        simulationSpeed = sim;
                        sim = 0;
                        lastFpsCheck = System.nanoTime();
                    }

                    long timeTaken = System.nanoTime() - currentTime;
                    if (targetTime > timeTaken) {
                        try {
                            Thread.sleep((targetTime - timeTaken) / 1000000);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        };
        cycle.setName("Gameloop");
        cycle.start();
    }

    public static float updateDelta() {
        return 1.0f / 1000000000 * targetTime;
    }

    public static World getWorld(String worldName) {
        World foundWorld = null;
        for (Dimension dimension : dimensions) {
            World world = dimension.getWorld(worldName);
            if (world != null) {
                foundWorld = world;
            }
        }
        return foundWorld;
    }

    public static void deleteWorld(String worldName) {
        for (Dimension dimension : dimensions) {
            World world = dimension.getWorld(worldName);
            if (world != null) {
                dimension.removeWorld(world);
                return;
            }
        }
    }

    public static Map<Integer, String> getTileVariants() {
        Map<Integer, String> types = new HashMap<Integer, String>();
        types.put(0, "Water");
        types.put(1, "Grass");
        types.put(2, "Gravel");
        return types;
    }

    public static int getTileSize() {
        return tileSize;
    }

}