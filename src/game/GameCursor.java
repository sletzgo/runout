package game;

import java.awt.Image;
import java.awt.Toolkit;

/**
 * GameCursor
 * 
 * @since 0.1.0
 * @author Lawrence Bensaid <lawrencebensaid@icloud.com>
 */
public class GameCursor {

    private static Image iconBuild = Toolkit.getDefaultToolkit().getImage("resources/ui/cursorBuild.png");
    private static Image iconDestroy = Toolkit.getDefaultToolkit().getImage("resources/ui/cursorDestroy.png");
    private GameCursorState state = GameCursorState.DEFAULT;

    public void setState(GameCursorState state) {
        this.state = state;
    }

    public Image getImage() {
        switch (state) {
        case DEFAULT:
            return null;
        case BUILD:
            return iconBuild;
        case DESTROY:
            return iconDestroy;
        default:
            return null;
        }
    }

    public enum GameCursorState {
        DEFAULT, BUILD, DESTROY
    }

}