package game;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

/**
 * Terrain is terrain...
 * 
 * @see game.World
 * @see game.Chunk
 * @see game.Tile
 * @since 0.1.0
 * @author Lawrence Bensaid <lawrencebensaid@icloud.com>
 */
public class Terrain {

    private final static Map<Integer, ResourcePath> tileIDs = new HashMap<Integer, ResourcePath>();
    private static boolean resourcesLoaded = false;

    public static boolean resourcesLoaded() {
        return resourcesLoaded;
    }

    public static void unloadResources() {
        tileIDs.clear();
        resourcesLoaded = false;
    }

    public static void loadResourcesIfNeeded() {
        if (!resourcesLoaded) {
            tileIDs.put(0, new ResourcePath("tile", "Water"));
            tileIDs.put(1, new ResourcePath("tile", "Grass"));
            tileIDs.put(2, new ResourcePath("tile", "Stone"));
            tileIDs.put(3, new ResourcePath("tile", "Sand"));
            resourcesLoaded = true;
        }
    }

    public static ResourcePath tileIDToResourcePath(int ID) {
        ResourcePath resourcePath = tileIDs.get(ID);
        return resourcePath;
    }

    public static Integer resourcePathToTileID(ResourcePath resourcePath) {
        Map<Integer, ResourcePath> map = new HashMap<Integer, ResourcePath>(tileIDs);
        Iterator<Entry<Integer, ResourcePath>> objectIterator = map.entrySet().iterator();
        while (objectIterator.hasNext()) {
            Map.Entry<Integer, ResourcePath> pair = (Map.Entry<Integer, ResourcePath>) objectIterator.next();
            Integer key = pair.getKey();
            ResourcePath value = pair.getValue();
            boolean found = false;
            if (value.equals(resourcePath)) {
                found = true;
            }
            objectIterator.remove();
            if (found) {
                return key;
            }
        }
        return null;
    }

    public static int[] getTileIDs() {
        ArrayList<Integer> list = new ArrayList<Integer>();
        Map<Integer, ResourcePath> map = new HashMap<Integer, ResourcePath>(tileIDs);
        Iterator<Entry<Integer, ResourcePath>> objectIterator = map.entrySet().iterator();
        while (objectIterator.hasNext()) {
            Map.Entry<Integer, ResourcePath> pair = (Map.Entry<Integer, ResourcePath>) objectIterator.next();
            list.add(pair.getKey());
            objectIterator.remove();
        }
        int[] array = new int[list.size()];
        for (int i = 0; i < array.length; i++) {
            array[i] = list.get(i);
        }
        Arrays.sort(array);
        return array;
    }

    /**
     * @param ID Tile identifier.
     * @param x  X coördinate in the scope of the {@link Chunk} (local coördinate).
     * @param y  Y coördinate in the scope of the {@link Chunk} (local coördinate).
     * @return tile.
     */
    public static Tile getTile(ResourcePath ID, int x, int y) {
        for (Tile tile : Tile.types()) {
            if (tile.getResourcePath().equals(ID)) {
                tile.meta(x, y);
                return tile;
            }
        }
        return null;
    }

    public static void generate(WorldPosition position, int range) {

    }

    private ArrayList<Chunk> chunks = new ArrayList<Chunk>();

    public Terrain(Tile[] tiles) {
        for (Tile tile : tiles) {
            boolean found = false;
            long tgx = tile.getGlobalX();
            long tgy = tile.getGlobalY();
            int tlx = Chunk.getLocalCordAt(tgx);
            int tly = Chunk.getLocalCordAt(tgy);
            int cx = Chunk.getChunkCordAt((int) tgx);
            int cy = Chunk.getChunkCordAt((int) tgy);
            for (int i = 0; i < chunks.size(); i++) {
                Chunk chunk = chunks.get(i);
                if (chunk.getX() == cx && chunk.getY() == cy) {
                    found = true;
                    break;
                }
            }
            if (!found) {
                chunks.add(new Chunk(cx, cy));
            }
            for (int i = 0; i < chunks.size(); i++) {
                Chunk chunk = chunks.get(i);
                if (chunk.getX() == cx && chunk.getY() == cy) {
                    chunk.setLocalTileAt(tile.getResourcePath(), tlx, tly);
                    chunks.set(i, chunk);
                    break;
                }
            }
        }
    }

    public Terrain(Chunk[] chunks) {
        for (Chunk chunk : chunks) {
            this.chunks.add(chunk);
        }
    }

    /**
     * @return width measured in tile size.
     */
    public int getWidth() {
        int leading = 0;
        int trailing = 0;
        for (Chunk chunk : chunks) {
            if (chunk.getX() * Chunk.SIZE < leading) {
                leading = chunk.getX();
            }
            if ((chunk.getX() * Chunk.SIZE) > trailing) {
                trailing = chunk.getX();
            }
        }
        return trailing - leading;
    }

    /**
     * @return height measured in tile size.
     */
    public int getHeight() {
        int top = 0;
        int bottom = 0;
        for (Chunk chunk : chunks) {
            if (chunk.getY() * Chunk.SIZE < top) {
                top = chunk.getY();
            }
            if ((chunk.getY() * Chunk.SIZE) > bottom) {
                bottom = chunk.getY();
            }
        }
        return bottom - top;
    }

    public WorldBiome getBiome(int gtx, int gty) {
        return new WorldBiome("StarZone");
    }

    /**
     * @return all chunks within the terrain
     */
    public Chunk[] getChunks() {
        return chunks.toArray(new Chunk[chunks.size()]);
    }

    /**
     * Adds a chunk to the terrain.
     * 
     * @param chunk The to-be-added Chunk.
     */
    public void addChunk(Chunk chunk) {
        chunks.add(chunk);
    }

    /**
     * Removes a chunk from the terrain.
     * 
     * @param cx Chunk-X-coordinate.
     * @param cy Chunk-Y-coordinate.
     */
    public void removeChunk(int cx, int cy) {
        ArrayList<Chunk> chunks = new ArrayList<Chunk>(this.chunks);
        for (Chunk chunk : chunks) {
            if (chunk.getX() == cx && chunk.getY() == cy) {
                this.chunks.remove(chunk);
            }
        }
    }

    /**
     * @return all tiles that belong to the terrain.
     */
    public Tile[] getTiles() {
        ArrayList<Tile> tiles = new ArrayList<Tile>();
        for (Chunk chunk : chunks) {
            Tile[] chunkTiles = chunk.getTiles();
            for (Tile tile : chunkTiles) {
                tiles.add(tile);
            }
        }
        return tiles.toArray(new Tile[tiles.size()]);
    }

    /**
     * @param X X coördinate in the scope of the {@link Terrain} (global
     *          coördinate).
     * @param Y Y coördinate in the scope of the {@link Terrain} (global
     *          coördinate).
     * @return tile.
     */
    public Chunk getChunkAt(int cx, int cy) {
        for (Chunk chunk : chunks) {
            if (chunk.getX() == cx && chunk.getY() == cy) {
                return chunk;
            }
        }
        return null;
    }

    /**
     * @param X X coördinate in the scope of the {@link Terrain} (global
     *          coördinate).
     * @param Y Y coördinate in the scope of the {@link Terrain} (global
     *          coördinate).
     * @return tile.
     */
    public Chunk getChunkAt(long gtx, long gty) {
        int cx = Chunk.getChunkCordAt(gtx);
        int cy = Chunk.getChunkCordAt(gty);
        for (Chunk chunk : chunks) {
            if (chunk.getX() == cx && chunk.getY() == cy) {
                return chunk;
            }
        }
        return null;
    }

}