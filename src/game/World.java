package game;

import java.util.ArrayList;

import core.Graphics;

/**
 * The world is the place where all the elements of the game come to life.
 * Players, Entities, Objects, Events, Triggers, Space & Time all come together
 * and interact with eachother.
 * 
 * @since 0.1.0
 * @author Lawrence Bensaid <lawrencebensaid@icloud.com>
 */
public class World {

    private String name;
    private WorldType type;
    private WorldTime time;
    private Terrain terrain;
    private ArrayList<Entity> entities;
    private ArrayList<WorldObject> objects;
    private ArrayList<Trigger> triggers;

    public World(String name, WorldTime time) {
        this(name, time, WorldType.OVERWORLD);
    }

    public World(String name, WorldTime time, WorldType type) {
        this(name, time, type, new Terrain(new Chunk[] { new Chunk(0, 0) }));
    }

    public World(String name, WorldTime time, WorldType type, Terrain terrain) {
        this(name, time, type, terrain, new ArrayList<WorldObject>());
    }

    public World(String name, WorldTime time, WorldType type, Terrain terrain, ArrayList<WorldObject> objects) {
        this(name, time, type, terrain, objects, new ArrayList<Entity>());
    }

    public World(String name, WorldTime time, WorldType type, Terrain terrain, ArrayList<WorldObject> objects,
            ArrayList<Entity> entities) {
        this(name, time, type, terrain, objects, entities, new ArrayList<Trigger>());
    }

    public World(String name, WorldTime time, WorldType type, Terrain terrain, ArrayList<WorldObject> objects,
            ArrayList<Entity> entities, ArrayList<Trigger> triggers) {
        this.name = name;
        this.time = time;
        this.type = type;
        this.terrain = terrain;
        this.objects = objects;
        this.entities = entities;
        this.triggers = triggers;
        System.out.println("World '" + this.name + "' was created!");
    }

    /**
     * @return the name of the world.
     */
    public String getName() {
        return name;
    }

    /**
     * @return the world type.
     */
    public WorldType getType() {
        return type;
    }

    /**
     * @return time of the world.
     */
    public WorldTime getTime() {
        return time;
    }

    /**
     * Spawns a given entity into the world.
     * 
     * @param entity entity to spawn.
     */
    public void spawn(Entity entity) {
        entities.add(entity);
    }

    /**
     * Spawns a given object into the world.
     * 
     * @param object object to spawn.
     */
    public void spawn(WorldObject object) {
        objects.add(object);
    }

    /**
     * Permanently removes a given entity from the world.
     * 
     * @param entity entity to remove.
     */
    public void despawn(Entity entity) {
        entities.remove(entity);
    }

    /**
     * Permanently removes a given object from the world.
     * 
     * @param object object to remove.
     */
    public void despawn(WorldObject object) {
        objects.remove(object);
    }

    /**
     * @return all the entities of the world.
     */
    public Entity[] getEntities() {
        return entities.toArray(new Entity[entities.size()]);
    }

    /**
     * @return all the objects of the world.
     */
    public WorldObject[] getObjects() {
        return objects.toArray(new WorldObject[objects.size()]);
    }

    /**
     * @return all the triggers of the world.
     */
    public Trigger[] getTriggers() {
        return triggers.toArray(new Trigger[triggers.size()]);
    }

    /**
     * @return the terrain that belongs to this world.
     */
    public Terrain getTerrain() {
        return terrain;
    }

    /**
     * @deprecated Interact via {@link #getTerrain} instead.
     * @param ID
     * @param x
     * @param y
     */
    // public void setTile(int ID, int x, int y) {
    // terrain.setTileAt(ID, x, y);
    // }

    /**
     * @deprecated Interact via {@link #getTerrain} instead.
     * @return
     */
    public Tile[] getTiles() {
        return terrain.getTiles();
    }

    /**
     * @param position reference point.
     * @return the nearest entity to the given position.
     */
    public Entity getNearestEntity(WorldPosition position) {
        float[] distances = new float[entities.size()];
        Entity[] entities = getEntities();
        Entity nearestEntity = null;
        float closestDistance = 0;
        for (int i = 0; i < entities.length; i++) {
            distances[i] = WorldPosition.getDistance(position, entities[i].getPosition());
        }
        if (distances.length > 0 && entities.length > 0) {
            closestDistance = distances[0];
            nearestEntity = entities[0];
            for (int i = 0; i < distances.length; i++) {
                if (closestDistance > distances[i]) {
                    closestDistance = distances[i];
                }
            }
        }
        return nearestEntity;
    }

    /**
     * @deprecated Interact via {@code getTerrain()} instead.
     * @param position reference point.
     * @return the biome of the given reference point.
     */
    public WorldBiome getBiome(WorldPosition position) {
        return new WorldBiome("Starzone");
    }

    public void onUpdate() {

        for (WorldObject object : objects) {
            object.onUpdate();
        }

        for (Entity entity : entities) {
            entity.onUpdate();
        }

    }

    public void onRender() {

        for (Chunk chunk : getTerrain().getChunks()) {
            int x = (int) chunk.getX();
            int y = (int) chunk.getY();
            int chunkX = x * Chunk.SIZE;
            int chunkY = y * Chunk.SIZE;
            for (int cx = 0; cx < Chunk.SIZE; cx++) {
                for (int cy = 0; cy < Chunk.SIZE; cy++) {
                    Tile tile = chunk.getLocalTileAt(cx, cy);
                    if (tile == null) {
                        continue;
                    }
                    float tileX = (chunkX + cx);
                    float tileY = (chunkY + cy);
                    Sprite sprite = tile.getSprite(chunk);
                    Graphics.drawImage(sprite.getImageResource(), tileX, tileY, 1f, 1f);
                }
            }
        }

        for (WorldObject object : objects) {
            object.onRender();
        }

        for (Entity entity : entities) {
            entity.onRender();
        }

    }

}