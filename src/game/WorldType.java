package game;

/**
 * WorldType
 * 
 * @see game.World
 * @since 0.1.0
 * @author Lawrence Bensaid <lawrencebensaid@icloud.com>
 */
public enum WorldType {
    OVERWORLD,
    HELLISH,
    VOID
}