package game;

/**
 * The ResourcePath is an identification method for game resources like Entity,
 * Tile & WorldObject.
 * 
 * @since 0.1.0
 * @author Lawrence Bensaid <lawrencebensaid@icloud.com>
 */
public class ResourcePath {

    private String namespace;
    private String resource;

    public ResourcePath(String namespace, String resource) {
        this.namespace = namespace.toLowerCase();
        this.resource = resource;
    }

    public String getNamespace() {
        return namespace;
    }

    public String getName() {
        return resource;
    }

    public boolean equals(String string) {
        return this.toString().equals(string);
    }

    @Override
    public boolean equals(Object obj) {
        try {
            ResourcePath resourcePath = (ResourcePath) obj;
            return resourcePath.namespace.equals(namespace) && resourcePath.resource.equals(resource);
        } catch (Exception e) {
            return super.equals(obj);
        }
    }

    public String toString(boolean full) {
        return (full ? namespace + "." : "") + resource;
    }

    @Override
    public String toString() {
        return toString(true);
    }

}
