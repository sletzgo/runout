package game;

import static game.WorldPosition.getDistance;
import static game.WorldPosition.getDirection;
import static util.RadiansUtil.directionToHorizontalVelocity;
import static util.RadiansUtil.directionToVerticalVelocity;

/**
 * Defines where and how much a {@code Client} can see.
 * 
 * @see net.Client
 * @see game.entity.Player
 * @since 0.1.0
 * @author Lawrence Bensaid <lawrencebensaid@icloud.com>
 */
public class Camera {

    private WorldPosition position;
    private WorldPosition target;
    private float viewport = 32;
    private float speed = 0;

    public Camera() {
        this(new WorldPosition());
    }

    public Camera(WorldPosition position) {
        this(position, 32);
    }

    public Camera(WorldPosition position, float viewport) {
        this.position = position;
        this.viewport = viewport;
        target = position;
    }

    /**
     * Sets the {@code viewport} to a specified value.
     * 
     * @see game.Tile
     * @param units amount of tiles (or units) a Client can see.
     */
    public void setViewport(float units) {
        viewport = units;
    }

    /**
     * @return the viewport value. (measured in tiles/units)
     */
    public float getViewport() {
        return viewport;
    }

    /**
     * Snaps the camera's X-coördinate to 'current value' + {@code addition}.
     * 
     * @param addition
     */
    public void moveX(float addition) {
        position.setX(position.getX() + addition);
    }

    /**
     * Snaps the camera's Y-coördinate to 'current value' + {@code addition}.
     * 
     * @param addition
     */
    public void moveY(float addition) {
        position.setY(position.getY() + addition);
    }

    /**
     * @return the X-coördinate.
     */
    public float getX() {
        return position.getX();
    }

    /**
     * @return the Y-coördinate.
     */
    public float getY() {
        return position.getY();
    }

    /**
     * @return the current camera speed.
     */
    public float getSpeed() {
        return speed;
    }

    /**
     * Sets where the camera needs to move towards.
     * 
     * @param target The camera's new destination.
     */
    public void goTo(WorldPosition target) {
        speed = 0;
        float step = getDistance(position, target);
        if (step > 0) {
            this.target = target;
            speed = step / viewport;
        }
    }

    public void onUpdate() {
        float direction = getDirection(position, target);
        position.setX(position.getX() + directionToHorizontalVelocity(direction, speed));
        position.setY(position.getY() + directionToVerticalVelocity(direction, speed));
    }

}