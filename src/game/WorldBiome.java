package game;

/**
 * A biome defines certain properties of an area of the world.
 * 
 * @see game.World
 * @since 0.1.0
 * @author Lawrence Bensaid <lawrencebensaid@icloud.com>
 */
public class WorldBiome {

    private final String name;
    private final WeatherCondition weather;

    public WorldBiome(String name) {
        this(name, new WeatherCondition());
    }

    public WorldBiome(String name, WeatherCondition weather) {
        this.name = name;
        this.weather = weather;
    }

    public String getName() {
        return name;
    }

    public WeatherCondition getWeatherCondition() {
        return weather;
    }

}
