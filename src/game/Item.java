package game;

/**
 * Items define the contents of an inventory.
 * 
 * @see game.Inventory
 * @since 0.1.0
 * @author Lawrence Bensaid <lawrencebensaid@icloud.com>
 */
public class Item {

    private final int ID;
    private final String name;
    private final String spriteName;

    public Item(int ID, String name, String spriteName) {
        this.ID = ID;
        this.name = name;
        this.spriteName = spriteName;
    }

    public int getID() {
        return ID;
    }

    public String getName() {
        return name;
    }

    public Sprite getSprite() {
        return new Sprite(spriteName);
    }

}