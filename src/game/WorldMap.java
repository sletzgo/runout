package game;

/**
 * The WorldMap represents the datastructure of the terrain.
 * 
 * @see game.Terrain
 * @deprecated has been replaced by {@link game.Terrain}.
 * @since 0.1.0
 * @author Lawrence Bensaid <lawrencebensaid@icloud.com>
 */
public class WorldMap {

	private int[][] terrain;

	public WorldMap() {
		this(new int[0][0]);
	}

	public WorldMap(int[][] terrain) {
		this.terrain = terrain;
		net.Server.print("Map created! (" + leadingEdge() + "; " + trailingEdge() + "; " + topEdge() + "; "
				+ bottomEdge() + ")");
	}

	public int[][] getMap() {
		return terrain;
	}

	public int leadingEdge() {
		return -(getWidth() / 2);
	}

	public int trailingEdge() {
		return getWidth() / 2;
	}

	public int topEdge() {
		return -(getHeight() / 2);
	}

	public int bottomEdge() {
		return getHeight() / 2;
	}

	public int getWidth() {
		return terrain.length;
	}

	public int getHeight() {
		return terrain[0].length;
	}

}
