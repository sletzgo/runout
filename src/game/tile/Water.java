package game.tile;

import game.Chunk;
import game.Sprite;
import game.Tile;

/**
 * Water
 * 
 * @since 0.1.0
 * @author Lawrence Bensaid <lawrencebensaid@icloud.com>
 */
public class Water extends Tile {

    public Water() {
        super("Water", new Sprite("tile.water"), new Sprite("tile.water-still"));
    }

    @Override
    public Sprite getSprite(Chunk chunk) {
        Tile topNeighbor = chunk.getLocalTileAt(lx, ly - 1);
        Tile topLeadingNeighbor = chunk.getLocalTileAt(lx - 1, ly - 1);
        Tile topTrailingNeighbor = chunk.getLocalTileAt(lx + 1, ly - 1);
        if (topNeighbor != null && topLeadingNeighbor != null) {
            String top = topNeighbor.getResourcePath().toString(false);
            String topLeading = topLeadingNeighbor.getResourcePath().toString(false);
            if (top.equals("Grass") && topLeading.equals("Water"))
                return new Sprite("tile.grass.cliff-left", "tile.water");
        }
        if (topNeighbor != null && topTrailingNeighbor != null) {
            String top = topNeighbor.getResourcePath().toString(false);
            String topTrailing = topTrailingNeighbor.getResourcePath().toString(false);
            if (top.equals("Grass") && topTrailing.equals("Water"))
                return new Sprite("tile.grass.cliff-right", "tile.water");
        }
        if (topNeighbor != null) {
            String top = topNeighbor.getResourcePath().toString(false);
            if (top.equals("Grass"))
                return new Sprite("tile.grass.cliff");
        }
        return sprite;
    }

}