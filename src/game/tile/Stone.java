package game.tile;

import game.Chunk;
import game.Sprite;
import game.Tile;

/**
 * Stone
 * 
 * @since 0.1.0
 * @author Lawrence Bensaid <lawrencebensaid@icloud.com>
 */
public class Stone extends Tile {

    public Stone() {
        super("Stone", new Sprite("tile.stone"));
    }

    @Override
    public Sprite getSprite(Chunk chunk) {
        // int gx = chunk.getGlobalXCordAt(lx);
        // int gy = chunk.getGlobalYCordAt(ly);
        // Tile topNeighbor = terrain.getTileAt(gx, gy - 1);
        // Tile topLeadingNeighbor = terrain.getTileAt(gx - 1, gy - 1);
        // Tile topTrailingNeighbor = terrain.getTileAt(gx + 1, gy - 1);
        // if (topNeighbor != null && topLeadingNeighbor != null)
        //     if (topNeighbor.getID() == 1 && topLeadingNeighbor.getID() == 0)
        //         return new Sprite("tile.grass.cliff-left", "tile.water");
        // if (topNeighbor != null && topTrailingNeighbor != null)
        //     if (topNeighbor.getID() == 1 && topTrailingNeighbor.getID() == 0)
        //         return new Sprite("tile.grass.cliff-right", "tile.water");
        // if (topNeighbor != null)
        //     if (topNeighbor.getID() == 1)
        //         return new Sprite("tile.grass.cliff");
        return sprite;
    }

}