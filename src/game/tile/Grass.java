package game.tile;

import game.Chunk;
import game.Sprite;
import game.Tile;

/**
 * Grass
 * 
 * @since 0.1.0
 * @author Lawrence Bensaid <lawrencebensaid@icloud.com>
 */
public class Grass extends Tile {

    public Grass() {
        super("Grass", new Sprite("tile.grass"));
    }

    @Override
    public Sprite getSprite(Chunk chunk) {
        String variant = "grass";
        Tile topNeighbor = chunk.getLocalTileAt(lx, ly - 1);
        Tile bottomNeighbor = chunk.getLocalTileAt(lx, ly + 1);
        Tile leadingNeighbor = chunk.getLocalTileAt(lx - 1, ly);
        Tile trailingNeighbor = chunk.getLocalTileAt(lx + 1, ly);
        if (topNeighbor != null && leadingNeighbor != null) {
            String top = topNeighbor.getResourcePath().toString(false);
            String leading = leadingNeighbor.getResourcePath().toString(false);
            if (top.equals("Water") && leading.equals("Water")) 
                return new Sprite("tile." + variant + ".top-left", "tile.water");
        }
        if (topNeighbor != null && trailingNeighbor != null) {
            String top = topNeighbor.getResourcePath().toString(false);
            String trailing = trailingNeighbor.getResourcePath().toString(false);
            if (top.equals("Water") && trailing.equals("Water"))
                return new Sprite("tile." + variant + ".top-right", "tile.water");
        }
        if (topNeighbor != null) {
            String top = topNeighbor.getResourcePath().toString(false);
            if (top.equals("Water"))
                return new Sprite("tile." + variant + ".top");
        }
        if (bottomNeighbor != null && leadingNeighbor != null) {
            String bottom = bottomNeighbor.getResourcePath().toString(false);
            String leading = leadingNeighbor.getResourcePath().toString(false);
            if (bottom.equals("Water") && leading.equals("Water"))
                return new Sprite("tile." + variant + ".bottom-left");
        }
        if (bottomNeighbor != null && trailingNeighbor != null) {
            String bottom = bottomNeighbor.getResourcePath().toString(false);
            String trailing = trailingNeighbor.getResourcePath().toString(false);
            if (bottom.equals("Water") && trailing.equals("Water"))
                return new Sprite("tile." + variant + ".bottom-right");
        }
        if (bottomNeighbor != null) {
            String bottom = bottomNeighbor.getResourcePath().toString(false);
            if (bottom.equals("Water"))
                return new Sprite("tile." + variant + "");
        }
        if (leadingNeighbor != null) {
            String leading = leadingNeighbor.getResourcePath().toString(false);
            if (leading.equals("Water"))
                return new Sprite("tile." + variant + ".left");
        }
        if (trailingNeighbor != null) {
            String trailing = trailingNeighbor.getResourcePath().toString(false);
            if (trailing.equals("Water"))
                return new Sprite("tile." + variant + ".right");
        }
        return sprite;
    }

}