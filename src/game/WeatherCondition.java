package game;

/**
 * WeatherCondition
 * 
 * @see game.WorldBiome
 * @since 0.1.0
 * @author Lawrence Bensaid <lawrencebensaid@icloud.com>
 */
public class WeatherCondition {

    private float chanceOfRain;
    private float chanceOfSnow;

    public WeatherCondition() {
        this(0, 0);
    }

    public WeatherCondition(float chanceOfRain, float chanceOfSnow) {
        this.chanceOfRain = chanceOfRain;
        this.chanceOfSnow = chanceOfSnow;
    }

    public float getChanceOfRain() {
        return chanceOfRain;
    }

    public float getChanceOfSnow() {
        return chanceOfSnow;
    }
    
}