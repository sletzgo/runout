package game.object;

import game.GameObject;
import game.Sprite;
import game.WorldObject;
import game.WorldPosition;

/**
 * PineTree
 * 
 * @since 0.1.0
 * @author Lawrence Bensaid <lawrencebensaid@icloud.com>
 */
public class PineTree extends WorldObject {

    public PineTree(WorldPosition position) {
        super("PineTree", new Sprite("object.tree.oak-summer" + (int) (Math.random() * 10)), new GameObject(position, 3, 4));

        String[] keys = new String[] {
            "object.tree.pine-summer",
            "object.tree.pine-summer-snow",
            "object.tree.pine-summer-snow-heavy",
            "object.tree.pine-winter",
            "object.tree.pine-winter-snow",
            "object.tree.pine-winter-snow-heavy",
            "object.tree.pine-spring",
            "object.tree.pine-autum",
        };

        this.sprite = new Sprite(keys[(int) (Math.random() * keys.length)]);
    }
    
}