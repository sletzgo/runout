package game.object;

import game.GameObject;
import game.Sprite;
import game.WorldObject;
import game.WorldPosition;

/**
 * Lantern
 * 
 * @since 0.1.0
 * @author Lawrence Bensaid <lawrencebensaid@icloud.com>
 */
public class Lantern extends WorldObject {

    public Lantern(WorldPosition position) {
        super("Lantern", new Sprite("object.lantern.x"), new GameObject(position, 3, 4));
    }

    @Override
    public Sprite getSprite() {
        if (this.getRotation() == 0) {
            return new Sprite("object.lantern.y");
        }
        return super.getSprite();
    }

}