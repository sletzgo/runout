package game.object;

import game.GameObject;
import game.Sprite;
import game.WorldObject;
import game.WorldPosition;

/**
 * Sign
 * 
 * @since 0.1.0
 * @author Lawrence Bensaid <lawrencebensaid@icloud.com>
 */
public class Sign extends WorldObject {

    public Sign(WorldPosition position) {
        super("Sign", new Sprite("object.sign"), new GameObject(position, 1, 3));
    }
    
}