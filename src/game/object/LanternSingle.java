package game.object;

import game.GameObject;
import game.Sprite;
import game.WorldObject;
import game.WorldPosition;

/**
 * LanternSingle
 * 
 * @since 0.1.0
 * @author Lawrence Bensaid <lawrencebensaid@icloud.com>
 */
public class LanternSingle extends WorldObject {

    public LanternSingle(WorldPosition position) {
        super("LanternSingle", new Sprite("object.lantern.single"), new GameObject(position, 1, 4));
    }

}