package game.object;

import game.GameObject;
import game.Sprite;
import game.WorldObject;
import game.WorldPosition;

/**
 * BirchTree
 * 
 * @since 0.1.0
 * @author Lawrence Bensaid <lawrencebensaid@icloud.com>
 */
public class BirchTree extends WorldObject {

    public BirchTree(WorldPosition position) {
        super("BirchTree", new Sprite("object.tree.oak-summer" + (int) (Math.random() * 10)), new GameObject(position, 3, 4));

        String[] keys = new String[] {
            "object.tree.birch-summer",
            "object.tree.birch-summer-snow",
            "object.tree.birch-summer-snow-heavy"
        };

        this.sprite = new Sprite(keys[(int) (Math.random() * keys.length)]);
    }
    
}