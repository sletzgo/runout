package game.object;

import game.GameObject;
import game.Sprite;
import game.WorldObject;
import game.WorldPosition;

/**
 * Stargate
 * 
 * @author Lawrence Bensaid <lawrencebensaid@icloud.com>
 */
public class Stargate extends WorldObject {

    public Stargate(WorldPosition position) {
        super("Stargate", new Sprite("object.stargate.engaged"), new GameObject(position, 3, 3));
    }

}