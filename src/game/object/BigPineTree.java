package game.object;

import game.GameObject;
import game.Sprite;
import game.WorldObject;
import game.WorldPosition;

/**
 * BigPineTree
 * 
 * @since 0.1.0
 * @author Lawrence Bensaid <lawrencebensaid@icloud.com>
 */
public class BigPineTree extends WorldObject {

    public BigPineTree(WorldPosition position) {
        super("BigPineTree", new Sprite("object.tree.oak-summer" + (int) (Math.random() * 10)), new GameObject(position, 3, 5));

        String[] keys = new String[] {
            "object.tree.bigpine-summer",
            "object.tree.bigpine-winter",
            "object.tree.bigpine-frozen",
            "object.tree.bigpine-autum",
            "object.tree.bigpine-spring"
        };

        this.sprite = new Sprite(keys[(int) (Math.random() * keys.length)]);
    }
    
}