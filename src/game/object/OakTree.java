package game.object;

import game.GameObject;
import game.Sprite;
import game.WorldObject;
import game.WorldPosition;

/**
 * Tree
 * 
 * @since 0.1.0
 * @author Lawrence Bensaid <lawrencebensaid@icloud.com>
 */
public class OakTree extends WorldObject {

    public OakTree(WorldPosition position) {
        super("OakTree", new Sprite("object.tree.oak-summer" + (int) (Math.random() * 10)), new GameObject(position, 3, 4));

        String[] keys = new String[] {
            "object.tree.oak-summer",
            "object.tree.oak-summer-snow",
            "object.tree.oak-summer-snow-heavy",
            "object.tree.oak-autum",
            "object.tree.oak-spring"
        };

        this.sprite = new Sprite(keys[(int) (Math.random() * keys.length)]);
    }
    
}