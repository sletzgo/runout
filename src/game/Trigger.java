package game;

/**
 * A trigger is an object that can be interacted with and as a result does
 * something. TODO: Implement triggers.
 * 
 * @since 0.1.0
 * @author Lawrence Bensaid <lawrencebensaid@icloud.com>
 */
public class Trigger {

    private WorldTime execution;
    private boolean executed = false;
    private String name;

    public Trigger() {
        this("Trigger");
    }

    public Trigger(String name) {
        this.name = name;
    }

    public WorldTime getExecutionTime() {
        return execution;
    }

    public void execute() {
        executed = true;
    }

    public boolean isExecuted() {
        return executed;
    }

    public String getName() {
        return name;
    }

}