package game;

/**
 * GameTime
 * 
 * @since 0.1.0
 * @author Lawrence Bensaid <lawrencebensaid@icloud.com>
 */
public class GameTime {

    private int step = 0;

    public GameTime() {

    }

    /**
     * Resets the game time.
     * 
     * @implNote Don't save after this point. Since it will completely reset the
     *           session's game time.
     */
    public void reset() {
        step = 0;
    }

    /**
     * Registers a tick.
     */
    public void tick() {
        step++;
    }

    /**
     * @return the current step (or time frame).
     */
    public int getStep() {
        return step;
    }

}