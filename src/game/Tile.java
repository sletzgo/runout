package game;

import java.awt.image.BufferedImage;
import java.util.ArrayList;

import game.tile.*;

/**
 * Tile buildingblocks for the chucks / terrain.
 * 
 * @see game.Chunk
 * @see game.Terrain
 * @see game.Sprite
 * @since 0.1.0
 * @author Lawrence Bensaid <lawrencebensaid@icloud.com>
 */
public class Tile {

	private final static ArrayList<Tile> TILES = new ArrayList<Tile>();
	private static boolean resourcesLoaded = false;

	public static boolean resourcesLoaded() {
		return resourcesLoaded;
	}

	public static void loadResourcesIfNeeded() {
		if (!resourcesLoaded) {
			TILES.add(new Water());
			TILES.add(new Grass());
			TILES.add(new Stone());
			TILES.add(new Sand());
			resourcesLoaded = true;
		}
	}

	public static Tile[] types() {
		return TILES.toArray(new Tile[TILES.size()]);
	}

	public static Tile create(ResourcePath resourcePath) {
		// TODO: Create from resourcepath
		switch (resourcePath.toString(false)) {
		case "Water":
			return new game.tile.Water();
		case "Grass":
			return new game.tile.Grass();
		case "Stone":
			return new game.tile.Stone();
		case "Sand":
			return new game.tile.Sand();
		default:
			return null;
		}
	}

    public static Tile createAbstractTile(String TYPE, long gx, long gy) {
        return new Tile(TYPE, gx, gy);
    }

    public static int previous(int ID) {
        int[] tileIDs = Terrain.getTileIDs();
        for (int i = 0; i < tileIDs.length; i++) {
            if (ID == tileIDs[i] && i - 1 >= 0) {
                return tileIDs[i - 1];
            }
        }
        return tileIDs.length > 0 ? tileIDs[tileIDs.length - 1] : null;
    }

    public static int next(int ID) {
        int[] tileIDs = Terrain.getTileIDs();
        for (int i = 0; i < tileIDs.length; i++) {
            if (ID == tileIDs[i] && i + 1 < tileIDs.length) {
                return tileIDs[i + 1];
            }
        }
        return tileIDs.length > 0 ? tileIDs[0] : null;
    }

	private final ResourcePath resourcePath;
    protected final Sprite sprite;
    protected final Sprite icon;
    private final boolean walkable = true;
    private Terrain terrain;
    protected int lx, ly;
    public long gx, gy;
    
    private Tile(String TYPE, long gx, long gy) {
        resourcePath = new ResourcePath("tile", TYPE);
        this.sprite = null;
        this.icon = null;
        this.gx = gx;
        this.gy = gy;
    }

    public Tile(String TYPE, Sprite sprite) {
        this(TYPE, sprite, null);
    }

    public Tile(String TYPE, Sprite sprite, Sprite icon) {
        resourcePath = new ResourcePath("tile", TYPE);
        this.sprite = sprite;
        this.icon = icon;
    }

    public Sprite getSprite(Chunk chunk) {
        return sprite;
    }

    public BufferedImage getIcon() {
        if (icon == null) {
            return sprite.getImage();
        }
        return icon.getImage();
    }

    /**
     * @param x X coördinate in the scope of the {@link Chunk} (local coördinate).
     * @param y Y coördinate in the scope of the {@link Chunk} (local coördinate).
     */
    public void meta(int x, int y) {
        this.lx = x;
        this.ly = y;
    }

    protected Terrain getTerrain() {
        return terrain;
    }

    public boolean isWalkable() {
        return walkable;
    }

    public String getName() {
        return sprite.getKey();
    }

    public void setGlobal(long X, long Y) {
        gx = X;
        gy = Y;
    }

    public long getGlobalX() {
        return gx;
    }

    public long getGlobalY() {
        return gy;
    }

	/**
	 * Result format example: {@code object.Tree} or {@code object.Door}.
	 * 
	 * @return type as a ResourcePath.
	 */
	public ResourcePath getResourcePath() {
		return resourcePath;
	}

    @Override
    public String toString() {
        return resourcePath.toString();
    }
    
}