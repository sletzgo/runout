package game;

import java.util.ArrayList;

/**
 * Dimension
 * 
 * @see game.World
 * @since 0.1.0
 * @author Lawrence Bensaid <lawrencebensaid@icloud.com>
 */
public class Dimension {

    private ArrayList<World> worlds = new ArrayList<World>();
    private ArrayList<Event> events = new ArrayList<Event>();
    private String name;

    public Dimension(World[] worlds) {
        this("Branch", worlds);
    }

    public Dimension(String[] worldNames) {
        this("Branch", worldNames);
    }

    public Dimension(String name, World[] worlds) {
        this.name = name;
        for (World world : worlds) {
            this.worlds.add(world);
        }
    }

    public Dimension(String name, ArrayList<World> worlds, ArrayList<Event> events) {
        this.name = name;
        this.worlds = worlds;
        this.events = events;
    }

    public Dimension(String name, World[] worlds, Event[] events) {
        this.name = name;
        for (World world : worlds) {
            this.worlds.add(world);
        }
        for (Event event : events) {
            this.events.add(event);
        }
    }

    public Dimension(String name, String[] worldNames) {
        this.name = name;
        for (String worldName : worldNames) {
            this.worlds.add(new World(worldName, new WorldTime()));
        }
    }

    public String getName() {
        return name;
    }

    public void addWorld(World world) {
        worlds.add(world);
    }

    public World getWorld(String name) {
        for (World world : worlds) {
            if (world.getName().equals(name)) {
                return world;
            }
        }
        return null;
    }

    public void removeWorld(World world) {
        worlds.remove(world);
    }

    public void removeWorld(String name) {
        worlds.remove(getWorld(name));
    }

    public World[] getWorlds() {
        return worlds.toArray(new World[worlds.size()]);
    }

    public void addEvent(Event event) {
        events.add(event);
    }

    public Event getEvent(String name) {
        for (Event event : events) {
            if (event.getName().equals(name)) {
                return event;
            }
        }
        return null;
    }

    public Event[] getEvents() {
        return events.toArray(new Event[events.size()]);
    }

    public void onUpdate() {
        for (World world : worlds) {
            world.onUpdate();
        }
    }

}