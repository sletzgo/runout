package game;

/**
 * Events are occurances of something. They are activated at a specified
 * timeframe. TODO: Implement events system.
 * 
 * @see game.GameTime
 * @see game.WorldTime
 * @since 0.1.0
 * @author Lawrence Bensaid <lawrencebensaid@icloud.com>
 */
public class Event {

    private String name;

    public Event() {
        this("New event");
    }

    public Event(String name) {
        this.name = name;
    }

    public String getName() {
        return this.name;
    }

}