package game;

import java.awt.Color;

/**
 * WorldSeason
 * 
 * @see game.WorldTime
 * @since 0.1.0
 * @author Lawrence Bensaid <lawrencebensaid@icloud.com>
 */
public class WorldSeason {

    private String name;
    private Color ui;
    private int duration;

    /**
     * @param name     Name of the season. For example: Summer
     * @param duration Duration in days.
     */
    public WorldSeason(String name, int duration) {
        this(name, duration, Color.yellow);
    }

    /**
     * @param name     Name of the season. For example: Summer
     * @param duration Duration in days.
     * @param ui       Color of the season on the player's HUD.
     */
    public WorldSeason(String name, int duration, Color ui) {
        this.name = name;
        this.ui = ui;
        this.duration = duration;
    }

    /**
     * @return the name of the season.
     */
    public String getName() {
        return name;
    }

    /**
     * @return the corresponding UI color.
     */
    public Color getUIColor() {
        return ui;
    }

    /**
     * @return the duration of a season measured in days.
     */
    public int getDuration() {
        return duration;
    }

    @Override
    public String toString() {
        return name;
    }

}