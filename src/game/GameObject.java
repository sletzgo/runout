package game;

import game.WorldPosition;

/**
 * GameObject
 * 
 * @since 0.1.0
 * @author Lawrence Bensaid <lawrencebensaid@icloud.com>
 */
public class GameObject {

    protected WorldPosition position;
	protected float width;
	protected float height;
    protected float speed;
    protected float speedMax;
	protected boolean solid;

    public GameObject(float xPos, float yPos) {
        this(xPos, yPos, 0);
    }

    public GameObject(float xPos, float yPos, float rotation) {
        this(xPos, yPos, rotation, null);
    }

    public GameObject(float xPos, float yPos, float rotation, String worldName) {
        this(new WorldPosition(xPos, yPos, rotation, worldName));
    }

    public GameObject(WorldPosition position) {
        this(position, 1, 1);
    }

    public GameObject(WorldPosition position, float width, float height) {
        this(position, width, height, 0);
    }

    public GameObject(WorldPosition position, float width, float height, float speed) {
		this.position = position;
		this.width = width;
		this.height = height;
		this.speed = speed;
        this.speedMax = 1;
        this.solid = true;
    }

	public WorldPosition getPosition() {
		return position;
	}

    /**
     * @return The width of the object.
     */
	public float getWidth() {
		return width;
	}

    /**
     * @
     * 
     * @return The height of the object.
     */
	public float getHeight() {
		return height;
	}

	public void isSolid(boolean solid) {
		this.solid = solid;
	}

	public boolean isSolid() {
		return solid;
	}

	public void setSpeed(float speed) {
		this.speed = speed;
	}

	public float getSpeed() {
		return speed;
	}

    public void setMaxSpeed(float speed) {
        this.speedMax = speed;
    }

	public float getMaxSpeed() {
		return speedMax;
	}

    public void setX(float x) {
        position.setX(x);
    }

    public float getX() {
        return position.x;
    }

    public void setY(float y) {
        position.setY(y);
    }

    public float getY() {
        return position.y;
    }

    public void setZ(float z) {
        position.setZ(z);
    }

    public float getZ() {
        return position.z;
    }

    public void setRotation(float rotation) {
        position.setRotation(rotation);
    }

    public float getRotation() {
        return position.direction;
    }

	public void onUpdate() {
		
	}

	public void onRender() {
		
	}
    
}