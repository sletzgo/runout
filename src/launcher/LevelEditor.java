package launcher;

import java.awt.Image;
import java.awt.FileDialog;
import java.awt.Dimension;
import java.awt.event.KeyEvent;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.io.File;
import java.io.FilenameFilter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JRadioButtonMenuItem;
import javax.swing.KeyStroke;
import javax.swing.JOptionPane;

import core.io.GameFile;
import game.Entity;
import game.Game;
import game.ResourcePath;
import game.Terrain;
import game.Tile;
import game.WorldObject;
import game.WorldPosition;
import levelbuilder.MiniMap;
import levelbuilder.BuildMode;

/**
 * The Level Editor is a seperate application used for editing the terrain of
 * worlds within a levels.
 * 
 * @since 0.1.0
 * @author Lawrence Bensaid <lawrencebensaid@icloud.com>
 */
@SuppressWarnings("serial")
public class LevelEditor extends JFrame implements ActionListener, ItemListener {

    private static Image icon = new ImageIcon("resources/ui/levelbuilderAppIcon.png").getImage();

    public static void main(String[] args) {
        new LevelEditor();
    }

    private ArrayList<JRadioButtonMenuItem> itemsWorlds = new ArrayList<JRadioButtonMenuItem>();
    private Map<ResourcePath, JRadioButtonMenuItem> itemsTiles = new HashMap<ResourcePath, JRadioButtonMenuItem>();
    private Map<ResourcePath, JRadioButtonMenuItem> itemsEntities = new HashMap<ResourcePath, JRadioButtonMenuItem>();
    private Map<ResourcePath, JRadioButtonMenuItem> itemsObjects = new HashMap<ResourcePath, JRadioButtonMenuItem>();
    private JMenu menuFile = new JMenu("File");
    private JMenu menuEdit = new JMenu("Edit");
    private JMenu menuView = new JMenu("View");
    private JMenu menuGame = new JMenu("Game");
    private JMenu menuWorld = new JMenu("World");
    private JMenu menuTerrain = new JMenu("Terrain");
    private JRadioButtonMenuItem itemTileTool = new JRadioButtonMenuItem("Terrain Tool");
    private JRadioButtonMenuItem itemEntityTool = new JRadioButtonMenuItem("Entity Tool");
    private JRadioButtonMenuItem itemObjectTool = new JRadioButtonMenuItem("Object Tool");
    private JRadioButtonMenuItem itemTriggerTool = new JRadioButtonMenuItem("Trigger Tool");
    private JMenuItem itemNewFile = new JMenuItem("New File");
    private JMenuItem itemOpen = new JMenuItem("Open...");
    private JMenuItem itemSave = new JMenuItem("Save");
    private JMenuItem itemSaveAs = new JMenuItem("Save As...");
    private JCheckBoxMenuItem itemShowRenderer = new JCheckBoxMenuItem("Renderer padding", true);
    private JCheckBoxMenuItem itemShowGrid = new JCheckBoxMenuItem("Render Grid", true);
    private JMenuItem itemZoomIn = new JMenuItem("Zoom In");
    private JMenuItem itemZoomOut = new JMenuItem("Zoom Out");
    private JMenuItem itemZoomReset = new JMenuItem("Reset Zoom");
    private JMenuItem itemGoToZero = new JMenuItem("Jump to center");
    private JMenuItem itemWorldDelete = new JMenuItem("Delete world");
    private JMenuItem itemGenerate = new JMenuItem("Start World Generation");
    private MiniMap board = new MiniMap(true, false, true);
    // private DefaultListModel<String> entityModel = new
    // DefaultListModel<String>();
    // private DefaultListModel<String> objectModel = new
    // DefaultListModel<String>();
    // private DefaultListModel<String> tileModel = new DefaultListModel<String>();
    // private JTabbedPane sidebar = new JTabbedPane();
    // private JList<String> entityList = new JList<String>(entityModel);
    // private JList<String> objectList = new JList<String>(objectModel);
    // private JList<String> tileList = new JList<String>(tileModel);
    // private JSplitPane splitpane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT,
    // true, board, sidebar);

    public LevelEditor() {
        System.setProperty("apple.laf.useScreenMenuBar", "true");
        board.setNotLoadedMessage("Press \"⌘O\" to open file");
        Game.initialiseIfNeeded();

        itemsWorlds.add(new JRadioButtonMenuItem("Overworld"));
        itemsWorlds.add(new JRadioButtonMenuItem("Testworld"));
        itemsWorlds.add(new JRadioButtonMenuItem("Nether"));

        for (Tile tile : Tile.types()) {
            JRadioButtonMenuItem item = new JRadioButtonMenuItem(tile.getResourcePath().toString(false));
            itemsTiles.put(tile.getResourcePath(), item);
        }

        for (WorldObject object : WorldObject.types()) {
            JRadioButtonMenuItem item = new JRadioButtonMenuItem(object.getResourcePath().toString(false));
            itemsObjects.put(object.getResourcePath(), item);
        }

        for (Entity entity : Entity.types()) {
            JRadioButtonMenuItem item = new JRadioButtonMenuItem(entity.getResourcePath().toString(false));
            itemsEntities.put(entity.getResourcePath(), item);
        }

        // for (File file : new File("./bin/game/object").listFiles()) {
        // String item = file.getName().split("\\.")[0];
        // objectModel.addElement(item);
        // }
        // for (File file : new File("./bin/game/entity").listFiles()) {
        // String item = file.getName().split("\\.")[0];
        // entityModel.addElement(item);
        // }
        // Map<Integer, String> map = Game.getTileVariants();
        // Iterator<Entry<Integer, String>> i = map.entrySet().iterator();
        // while (i.hasNext()) {
        // Map.Entry<Integer, String> pair = (Map.Entry<Integer, String>) i.next();
        // tileModel.addElement(pair.getKey() + ": " + pair.getValue());
        // i.remove();
        // }

        // for (Tile tile : Tile.getAll()) {
        // tileModel.addElement(tile.getID() + ": " + tile.getName());
        // }

        // sidebar.setBackground(new Color(59, 62, 65));
        // sidebar.setBorder(new EmptyBorder(0, 0, 0, 0));
        // sidebar.addTab("Objects", objectList);
        // sidebar.addTab("Entities", entityList);
        // sidebar.addTab("Tiles", tileList);
        // sidebar.setPreferredSize(new Dimension(100, 100));
        // splitpane.setBorder(new EmptyBorder(0, 0, 0, 0));
        // splitpane.setResizeWeight(0.75);

        setupWindow();

        // splitpane.setDividerLocation(getWidth() - 250);

        update();
        chooseFile();
    }

    private void setupWindow() {
        setTitle("Level editor");
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setSize(800, 550);
        setMinimumSize(new Dimension(400, 250));
        setLocationRelativeTo(null);
        setIconImage(icon);
        setContentPane(board);
        setupToolbar();
        setVisible(true);
    }

    private void setupToolbar() {
        JMenuBar toolbar = new JMenuBar();
        toolbar.add(menuFile);
        toolbar.add(menuEdit);
        toolbar.add(menuView);
        toolbar.add(menuGame);
        toolbar.add(menuWorld);
        toolbar.add(menuTerrain);

        // File
        itemNewFile.setEnabled(false);
        itemNewFile.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_N, ActionEvent.META_MASK));
        itemOpen.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_O, ActionEvent.META_MASK));
        itemSave.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S, ActionEvent.META_MASK));
        itemSaveAs.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S, ActionEvent.SHIFT_MASK));
        menuFile.add(itemNewFile);
        menuFile.addSeparator();
        menuFile.add(itemOpen);
        menuFile.addSeparator();
        menuFile.add(itemSave);
        menuFile.add(itemSaveAs);

        // Edit
        JMenu itemTileSelect = new JMenu("Tile Brush");
        JMenu itemObjectSelect = new JMenu("Object Brush");
        JMenu itemEntitySelect = new JMenu("Entity Brush");
        ButtonGroup group = new ButtonGroup();
        ButtonGroup groupTileSelect = new ButtonGroup();
        ButtonGroup groupObjectSelect = new ButtonGroup();
        ButtonGroup groupEntitySelect = new ButtonGroup();
        itemTileTool.setSelected(true);
        itemTileTool.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_1, ActionEvent.META_MASK));
        itemEntityTool.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_2, ActionEvent.META_MASK));
        itemObjectTool.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_3, ActionEvent.META_MASK));
        itemTriggerTool.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_4, ActionEvent.META_MASK));
        group.add(itemTileTool);
        group.add(itemEntityTool);
        group.add(itemObjectTool);
        group.add(itemTriggerTool);
        menuEdit.add(itemTileTool);
        menuEdit.add(itemEntityTool);
        menuEdit.add(itemObjectTool);
        menuEdit.add(itemTriggerTool);
        menuEdit.addSeparator();
        menuEdit.add(itemTileSelect);
        menuEdit.add(itemObjectSelect);
        menuEdit.add(itemEntitySelect);

        // Add dynamic items
        Map<ResourcePath, JRadioButtonMenuItem> mapTiles = new HashMap<ResourcePath, JRadioButtonMenuItem>(itemsTiles);
        Iterator<Entry<ResourcePath, JRadioButtonMenuItem>> tileIterator = mapTiles.entrySet().iterator();
        while (tileIterator.hasNext()) {
            Map.Entry<ResourcePath, JRadioButtonMenuItem> pair = (Map.Entry<ResourcePath, JRadioButtonMenuItem>) tileIterator
                    .next();
            groupTileSelect.add(pair.getValue());
            itemTileSelect.add(pair.getValue());
            pair.getValue().addItemListener(this);
            tileIterator.remove();
        }
        Map<ResourcePath, JRadioButtonMenuItem> mapObjects = new HashMap<ResourcePath, JRadioButtonMenuItem>(
                itemsObjects);
        Iterator<Entry<ResourcePath, JRadioButtonMenuItem>> objectIterator = mapObjects.entrySet().iterator();
        while (objectIterator.hasNext()) {
            Map.Entry<ResourcePath, JRadioButtonMenuItem> pair = (Map.Entry<ResourcePath, JRadioButtonMenuItem>) objectIterator
                    .next();
            groupObjectSelect.add(pair.getValue());
            itemObjectSelect.add(pair.getValue());
            pair.getValue().addItemListener(this);
            objectIterator.remove();
        }
        Map<ResourcePath, JRadioButtonMenuItem> mapEntities = new HashMap<ResourcePath, JRadioButtonMenuItem>(
                itemsEntities);
        Iterator<Entry<ResourcePath, JRadioButtonMenuItem>> entityIterator = mapEntities.entrySet().iterator();
        while (entityIterator.hasNext()) {
            Map.Entry<ResourcePath, JRadioButtonMenuItem> pair = (Map.Entry<ResourcePath, JRadioButtonMenuItem>) entityIterator
                    .next();
            groupEntitySelect.add(pair.getValue());
            itemEntitySelect.add(pair.getValue());
            pair.getValue().addItemListener(this);
            entityIterator.remove();
        }

        // View
        itemShowRenderer.setEnabled(true);
        itemShowGrid.setEnabled(true);
        itemShowRenderer
                .setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_R, KeyEvent.META_MASK | KeyEvent.SHIFT_MASK));
        itemShowGrid.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_G, KeyEvent.META_MASK | KeyEvent.SHIFT_MASK));
        itemZoomIn.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_EQUALS, KeyEvent.META_MASK | KeyEvent.SHIFT_MASK));
        itemZoomOut.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_SUBTRACT, ActionEvent.META_MASK));
        menuView.add(itemShowRenderer);
        menuView.add(itemShowGrid);
        menuView.addSeparator();
        menuView.add(itemGoToZero);
        menuView.addSeparator();
        menuView.add(itemZoomIn);
        menuView.add(itemZoomOut);
        menuView.add(itemZoomReset);

        // Game
        JMenu itemWorlds = new JMenu("World");
        ButtonGroup groupWorld = new ButtonGroup();
        for (JRadioButtonMenuItem item : itemsWorlds) {
            groupWorld.add(item);
            itemWorlds.add(item);
        }
        menuGame.add(itemWorlds);

        // World
        menuWorld.add(itemWorldDelete);

        // Terrain
        itemGenerate.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_G, ActionEvent.META_MASK));
        menuTerrain.add(itemGenerate);

        // Listeners
        itemOpen.addActionListener(this);
        itemSave.addActionListener(this);
        itemSaveAs.addActionListener(this);
        itemTileTool.addItemListener(this);
        itemEntityTool.addItemListener(this);
        itemObjectTool.addItemListener(this);
        itemTriggerTool.addItemListener(this);
        itemShowRenderer.addItemListener(this);
        itemShowGrid.addItemListener(this);
        itemGoToZero.addActionListener(this);
        itemZoomIn.addActionListener(this);
        itemZoomOut.addActionListener(this);
        itemZoomReset.addActionListener(this);
        itemWorldDelete.addActionListener(this);
        itemGenerate.addActionListener(this);
        for (JRadioButtonMenuItem item : itemsWorlds) {
            item.addItemListener(this);
        }

        setJMenuBar(toolbar);
    }

    private void update() {
        if (Game.isLoaded()) {
            itemSave.setEnabled(true);
            itemSaveAs.setEnabled(true);
            menuEdit.setEnabled(true);
            menuView.setEnabled(true);
            menuGame.setEnabled(true);
            menuWorld.setEnabled(true);
            menuTerrain.setEnabled(true);
        } else {
            itemSave.setEnabled(false);
            itemSaveAs.setEnabled(false);
            menuEdit.setEnabled(false);
            menuView.setEnabled(false);
            menuGame.setEnabled(false);
            menuWorld.setEnabled(false);
            menuTerrain.setEnabled(false);
        }
    }

    private void chooseFile() {
        System.setProperty("apple.awt.fileDialogForDirectories", "false");
        FileDialog chooser = new FileDialog(this);
        chooser.setMode(FileDialog.LOAD);
        chooser.setTitle("Select file or folder");
        chooser.setFilenameFilter(new FilenameFilter() {
            @Override
            public boolean accept(File dir, String name) {
                return name.endsWith(".rlvl");
            }
        });
        chooser.setVisible(true);
        GameFile.load(chooser.getFiles());
        board.load();
        update();
    }

    @Override
    public void itemStateChanged(ItemEvent e) {
        for (JRadioButtonMenuItem item : itemsWorlds) {
            if (e.getSource() == item && e.getStateChange() == 1) {
                board.load(item.getText());
                break;
            }
        }
        if (e.getSource() == itemTileTool && e.getStateChange() == 1) {
            board.setMode(BuildMode.TERRAIN);
        }
        if (e.getSource() == itemEntityTool && e.getStateChange() == 1) {
            board.setMode(BuildMode.ENTITY);
        }
        if (e.getSource() == itemObjectTool && e.getStateChange() == 1) {
            board.setMode(BuildMode.OBJECT);
        }
        if (e.getSource() == itemTriggerTool && e.getStateChange() == 1) {
            board.setMode(BuildMode.TRIGGER);
        }
        Map<ResourcePath, JRadioButtonMenuItem> mapTiles = new HashMap<ResourcePath, JRadioButtonMenuItem>(itemsTiles);
        Iterator<Entry<ResourcePath, JRadioButtonMenuItem>> tileIterator = mapTiles.entrySet().iterator();
        while (tileIterator.hasNext()) {
            Map.Entry<ResourcePath, JRadioButtonMenuItem> pair = tileIterator.next();
            if (e.getSource() == pair.getValue() && e.getStateChange() == 1) {
                board.setTileBrush(pair.getKey());
            }
            tileIterator.remove();
        }
        Map<ResourcePath, JRadioButtonMenuItem> mapObjects = new HashMap<ResourcePath, JRadioButtonMenuItem>(
                itemsObjects);
        Iterator<Entry<ResourcePath, JRadioButtonMenuItem>> objectIterator = mapObjects.entrySet().iterator();
        while (objectIterator.hasNext()) {
            Map.Entry<ResourcePath, JRadioButtonMenuItem> pair = objectIterator.next();
            if (e.getSource() == pair.getValue() && e.getStateChange() == 1) {
                board.setObjectBrush(pair.getKey());
            }
            objectIterator.remove();
        }
        Map<ResourcePath, JRadioButtonMenuItem> mapEntities = new HashMap<ResourcePath, JRadioButtonMenuItem>(
                itemsEntities);
        Iterator<Entry<ResourcePath, JRadioButtonMenuItem>> entityIterator = mapEntities.entrySet().iterator();
        while (entityIterator.hasNext()) {
            Map.Entry<ResourcePath, JRadioButtonMenuItem> pair = entityIterator.next();
            if (e.getSource() == pair.getValue() && e.getStateChange() == 1) {
                board.setEntityBrush(pair.getKey());
            }
            entityIterator.remove();
        }
        if (e.getSource() == itemShowRenderer) {
            board.showRenderer(e.getStateChange() == 1);
        }
        if (e.getSource() == itemShowGrid) {
            board.showGrid(e.getStateChange() == 1);
        }
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == itemSaveAs || e.getSource() == itemSave) {
            System.setProperty("apple.awt.fileDialogForDirectories", "false");
            FileDialog chooser = new FileDialog(this);
            chooser.setFile("Runoutsave.rlvl");
            chooser.setMode(FileDialog.SAVE);
            chooser.setTitle("Select file or folder");
            chooser.setFilenameFilter(new FilenameFilter() {
                @Override
                public boolean accept(File dir, String name) {
                    return name.endsWith(".rlvl");
                }
            });
            chooser.setVisible(true);
            System.out.println(chooser.getDirectory() + " ||| " + chooser.getFile());
            GameFile.save(chooser.getDirectory() + chooser.getFile());
        }
        if (e.getSource() == itemOpen) {
            chooseFile();
        }
        if (e.getSource() == itemZoomIn) {
            board.zoomIn();
        }
        if (e.getSource() == itemZoomOut) {
            board.zoomOut();
        }
        if (e.getSource() == itemZoomReset) {
            board.setZoom();
        }
        if (e.getSource() == itemGoToZero) {
            board.goTo(0, 0);
        }
        if (e.getSource() == itemWorldDelete) {
            String name = board.getLoadedWorld().getName();
            String title = "Delete World '" + name + "'";
            String message = "Are you sure that you want to delete World '" + name
                    + "'? This action CAN NOT BE UNDONE!";
            int optionType = JOptionPane.YES_NO_OPTION;
            int messageType = JOptionPane.WARNING_MESSAGE;
            int reply = JOptionPane.showConfirmDialog(this, message, title, optionType, messageType);
            if (reply == JOptionPane.YES_OPTION) {
                board.unload();
                board.load();
                Game.deleteWorld(name);
            }
        }
        if (e.getSource() == itemGenerate) {
            Entity entity = board.getLoadedWorld().getNearestEntity(new WorldPosition());
            if (entity == null) {
                JOptionPane.showMessageDialog(this, "No player found!", "World Generator", JOptionPane.WARNING_MESSAGE);
                return;
            }
            Terrain.generate(entity.getPosition(), 4);
        }
    }

}