package launcher;

import gui.ServerGUI;

/**
 * Server
 */
public class Server {

    public Server() {
        new ServerGUI();
        net.Server.start();
    }

    public static void main(String args[]) throws Exception {
        new Server();
    }

}