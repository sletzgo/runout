package launcher;

import core.engine.Engine;
import core.io.GameFile;
import game.Game;

public class Local {

	public Local() {
		Game.initialiseIfNeeded();
		GameFile.load("Runoutsave");
		Game.start();
		Engine.start();
	}

	public static void main(String[] args) {
		new Local();
	}

}
