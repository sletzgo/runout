package util;

/**
 * Format
 * 
 * @since 0.1.0
 * @author Lawrence Bensaid <lawrencebensaid@icloud.com>
 */
public class Format {

	public static String boolToString(boolean b) {
		if (b) {
			return "true";
		} else {
			return "false";
		}
	}

}
