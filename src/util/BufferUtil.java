package util;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

/**
 * BufferUtil
 * 
 * @since 0.1.0
 * @author Lawrence Bensaid <lawrencebensaid@icloud.com>
 */
public class BufferUtil {

    public static byte[] objectToBytes(Object object) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        ObjectOutputStream out = null;
        byte[] bytes = null;
        try {
            out = new ObjectOutputStream(stream);
            out.writeObject(object);
            out.flush();
            bytes = stream.toByteArray();
        } catch (IOException exception) {
            System.out.println("Unable to modulate: " + exception.getMessage());
        } finally {
            try {
                stream.close();
            } catch (IOException exception) {
                System.out.println("Unable to close stream: " + exception.getMessage());
            }
        }
        return bytes;
    }

    public static Object bytesToObject(Byte[] bytes) {
        byte[] cBytes = new byte[bytes.length];
        for (int i = 0; i < bytes.length; i++) {
            cBytes[i] = bytes[i];
        }
        return bytesToObject(cBytes);
    }

    public static Object bytesToObject(byte[] bytes) {
        ByteArrayInputStream stream = new ByteArrayInputStream(bytes);
        ObjectInput in = null;
        Object object = null;
        try {
            in = new ObjectInputStream(stream);
            object = in.readObject();
        } catch (IOException exception) {
            System.out.println("Unable to modulate: " + exception.getMessage());
        } catch (ClassNotFoundException exception) {
            System.out.println("Unable to modulate class: " + exception.getMessage());
        } finally {
            try {
                if (in != null) {
                    in.close();
                }
            } catch (IOException exception) {
                System.out.println(exception.getMessage());
            }
        }
        return object;
    }

}