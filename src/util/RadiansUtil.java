package util;

/**
 * RadiansUtil
 * 
 * @since 0.1.0
 * @author Lawrence Bensaid <lawrencebensaid@icloud.com>
 */
public class RadiansUtil {

	public static float getAngle(float vx, float vy) {
	    return (float) Math.toDegrees(Math.atan2(vy, vx));
	}
	
	public static float directionToHorizontalVelocity(float angle, float velocity) {
	    return (float) (Math.cos(Math.toRadians(angle)) * velocity);
	}	
	
	public static float directionToVerticalVelocity(float direction, float velocity) {
		return (float) (Math.cos(Math.toRadians(direction + -90)) * velocity);
	}
	
}
