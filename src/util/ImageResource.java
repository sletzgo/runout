package util;

import java.awt.image.BufferedImage;
import java.awt.Graphics2D;
import java.awt.Image;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

import javax.imageio.ImageIO;

import core.Window;

import com.jogamp.opengl.util.texture.Texture;
import com.jogamp.opengl.util.texture.awt.AWTTextureIO;

/**
 * ImageResource
 * 
 * @since 0.1.0
 * @author Lawrence Bensaid <lawrencebensaid@icloud.com>
 */
public class ImageResource {

	private Texture texture = null;

	private BufferedImage image = null;

	public ImageResource(String path) {
		URL url = ImageResource.class.getResource("../");

		String filePath = String.join("/", ArrayHelper.pop(url.toString().split("/"))) + "/resources/" + path;

		try {
			image = ImageIO.read(new URL(filePath));
		} catch (MalformedURLException e) {
			System.out.println("Malformed URL " + filePath);
		} catch (IOException e) {
			System.out.println("IO Failure " + filePath);
		}

		if (image != null) {
			image.flush();
		}
	}

	public ImageResource(Image image) {
		this.image = toBufferedImage(image);
	}

	public Image getImage() {
		return image;
	}

	public Texture getTexture() {
		if (image == null) {
			return null;
		}

		if (texture == null) {
			texture = AWTTextureIO.newTexture(Window.getProfile(), image, true);
		}

		return texture;
	}

	/**
	 * Converts a given Image into a BufferedImage.
	 *
	 * @param image The Image to be converted
	 * @return The converted BufferedImage
	 */
	public static BufferedImage toBufferedImage(Image image) {
		if (image instanceof BufferedImage) {
			return (BufferedImage) image;
		}
		BufferedImage bimage = null;
		try {
			bimage = new BufferedImage(image.getWidth(null), image.getHeight(null), BufferedImage.TYPE_INT_ARGB);
			Graphics2D bGr = bimage.createGraphics();
			bGr.drawImage(image, 0, 0, null);
			bGr.dispose();
		} catch (IllegalArgumentException e) {
		}
		return bimage;
	}

}