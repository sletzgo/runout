package gui;

import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import core.engine.Engine;
import core.io.GameFile;
import game.Game;
import net.Server;

/**
 * ClientGUI
 */
@SuppressWarnings("serial")
public class ClientGUI extends JFrame {

    private static JTextField nameField = new JTextField();
    private static JTextField hostField = new JTextField("127.0.0.1");
    private static JTextField portField = new JTextField(Server.DEFAULT_PORT + "");

    public ClientGUI() {

        setupLayout();
    }

    private void setupLayout() {
        JPanel rootPanel = new JPanel();
        rootPanel.setLayout(new GridLayout(3, 2, 24, 0));
        rootPanel.setBorder(new EmptyBorder(24, 24, 24, 24));
        rootPanel.add(new JLabel("Name"));
        rootPanel.add(nameField);
        rootPanel.add(new JLabel("Host"));
        rootPanel.add(hostField);
        rootPanel.add(new JLabel("Port"));
        rootPanel.add(portField);

        ActionListener submitAction = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent event) {
                String name = nameField.getText();
                String host = hostField.getText();
                int port = Integer.parseInt(portField.getText());
                net.Client.connect(name, host, port);
                Game.initialiseIfNeeded();
                GameFile.load("Sample");
                Game.start();
                Engine.start();
                setVisible(false);
            }
        };
        nameField.addActionListener(submitAction);
        hostField.addActionListener(submitAction);
        portField.addActionListener(submitAction);

        setContentPane(rootPanel);
        setMinimumSize(new Dimension(550, 400));
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setSize(550, 400);
        setTitle("Runout");
        setVisible(true);
    }
    
}