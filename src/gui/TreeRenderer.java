package gui;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JTree;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeCellRenderer;

import java.awt.Component;
import java.awt.Image;

@SuppressWarnings("serial")
public class TreeRenderer extends DefaultTreeCellRenderer {

    private static Icon dimensionsIcon, dimensionIcon, worldsIcon, worldIcon, objectsIcon, objectIcon, entitiesIcon, entityIcon, eventsIcon, eventIcon, triggersIcon, triggerIcon;

    public TreeRenderer() {
        dimensionsIcon = resizeIcon(new ImageIcon("resources/ui/iconDimensions.png"), 16, 16);
        dimensionIcon = resizeIcon(new ImageIcon("resources/ui/iconGate.png"), 16, 16);
        worldsIcon = resizeIcon(new ImageIcon("resources/ui/iconDots.png"), 16, 16);
        worldIcon = resizeIcon(new ImageIcon("resources/ui/iconGlobe.png"), 16, 16);
        objectsIcon = resizeIcon(new ImageIcon("resources/ui/iconObjects.png"), 16, 16);
        objectIcon = resizeIcon(new ImageIcon("resources/ui/iconModel.png"), 16, 16);
        entitiesIcon = resizeIcon(new ImageIcon("resources/ui/iconEntities.png"), 16, 16);
        entityIcon = resizeIcon(new ImageIcon("resources/ui/iconEntity1.png"), 16, 16);
        eventsIcon = resizeIcon(new ImageIcon("resources/ui/iconSettings.png"), 16, 16);
        eventIcon = resizeIcon(new ImageIcon("resources/ui/iconAlarm.png"), 16, 16);
        triggersIcon = resizeIcon(new ImageIcon("resources/ui/iconTrigger.png"), 16, 16);
        triggerIcon = resizeIcon(new ImageIcon("resources/ui/iconTriggering.png"), 16, 16);
    }

    @Override
    public Component getTreeCellRendererComponent(JTree tree, Object value, boolean sel, boolean exp, boolean leaf,
            int row, boolean hasFocus) {
        DefaultMutableTreeNode node = (DefaultMutableTreeNode) value;
        if ("Load game...".equals(node.toString())) {
            setLeafIcon(getDefaultLeafIcon());
        } else if ("Dimensions".equals(node.toString())) {
            setOpenIcon(dimensionsIcon);
            setClosedIcon(dimensionsIcon);
            setLeafIcon(dimensionsIcon);
        } else if ("Dimensions".equals(node.getParent().toString())) {
            setOpenIcon(dimensionIcon);
            setClosedIcon(dimensionIcon);
            setLeafIcon(dimensionIcon);
        } else if ("Worlds".equals(node.toString())) {
            setOpenIcon(worldsIcon);
            setClosedIcon(worldsIcon);
            setLeafIcon(worldsIcon);
        } else if ("Worlds".equals(node.getParent().toString())) {
            setOpenIcon(worldIcon);
            setClosedIcon(worldIcon);
            setLeafIcon(worldIcon);
        } else if ("Objects".equals(node.toString())) {
            setOpenIcon(objectsIcon);
            setClosedIcon(objectsIcon);
            setLeafIcon(objectsIcon);
        } else if ("Objects".equals(node.getParent().toString())) {
            setOpenIcon(objectIcon);
            setClosedIcon(objectIcon);
            setLeafIcon(objectIcon);
        } else if ("Events".equals(node.toString())) {
            setOpenIcon(eventsIcon);
            setClosedIcon(eventsIcon);
            setLeafIcon(eventsIcon);
        } else if ("Events".equals(node.getParent().toString())) {
            setOpenIcon(eventIcon);
            setClosedIcon(eventIcon);
            setLeafIcon(eventIcon);
        } else if ("Entities".equals(node.toString())) {
            setOpenIcon(entitiesIcon);
            setClosedIcon(entitiesIcon);
            setLeafIcon(entitiesIcon);
        } else if ("Entities".equals(node.getParent().toString())) {
            setOpenIcon(entityIcon);
            setClosedIcon(entityIcon);
            setLeafIcon(entityIcon);
        } else if ("Triggers".equals(node.toString())) {
            setOpenIcon(triggersIcon);
            setClosedIcon(triggersIcon);
            setLeafIcon(triggersIcon);
        } else if ("Triggers".equals(node.getParent().toString())) {
            setOpenIcon(triggerIcon);
            setClosedIcon(triggerIcon);
            setLeafIcon(triggerIcon);
        } else {
            setOpenIcon(getDefaultOpenIcon());
            setClosedIcon(getDefaultClosedIcon());
            setLeafIcon(getDefaultLeafIcon());
        }
        super.getTreeCellRendererComponent(tree, value, sel, exp, leaf, row, hasFocus);
        return this;
    }

    private static Icon resizeIcon(ImageIcon icon, int resizedWidth, int resizedHeight) {
        Image img = icon.getImage();
        Image resizedImage = img.getScaledInstance(resizedWidth, resizedHeight, java.awt.Image.SCALE_SMOOTH);
        return new ImageIcon(resizedImage);
    }
}