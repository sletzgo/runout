package gui;

import java.awt.FileDialog;
import java.awt.GridLayout;
import java.awt.Dimension;
import java.awt.event.KeyEvent;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.io.File;
import java.io.FilenameFilter;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import core.io.GameFile;
import game.Game;
import net.ServerClient;

/**
 * ServerGUI
 */
@SuppressWarnings("serial")
public class ServerGUI extends JFrame implements ActionListener, ItemListener {

    public static ServerGUI instance;

    private DefaultListModel<ServerClient> clients = new DefaultListModel<ServerClient>();
    public TerminalView terminalView = new TerminalView();
    public ControlView tabView = new ControlView();
    private ClientView clientView = new ClientView(clients);
    private TreeView treeView = new TreeView();

    // Toolbar items
    private JCheckBoxMenuItem itemFog = new JCheckBoxMenuItem("Fog of war");
    private JMenuItem itemDisconnect = new JMenuItem("Disconnect");
    private JMenuItem itemLoad = new JMenuItem("Load...");
    private JMenuItem itemSave = new JMenuItem("Save...");
    private JMenuItem itemStart = new JMenuItem("Start");
    private JMenuItem itemStop = new JMenuItem("Stop");

    public ServerGUI() {
        System.setProperty("apple.laf.useScreenMenuBar", "true");
        Game.initialiseIfNeeded();

        instance = this;

        // Logic
        tabView.optionDisconnect.setEnabled(false);
        clientView.addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent event) {
                if (!event.getValueIsAdjusting()) {
                    // Item is selected
                    tabView.optionDisconnect.setEnabled(true);
                }
            }
        });

        setupToolbar();

        setupLayout();

        setupActions();
    }

    private void setupLayout() {
        // Root layout
        JPanel rootPanel = new JPanel();
        JPanel centerPanel = new JPanel();
        JPanel leftPanel = new JPanel();
        JPanel rightPanel = new JPanel();
        JPanel bottomPanel = new JPanel();
        JSplitPane horizontal1 = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, centerPanel, rightPanel);
        JSplitPane horizontal2 = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, leftPanel, horizontal1);
        JSplitPane vertical1 = new JSplitPane(JSplitPane.VERTICAL_SPLIT, horizontal2, bottomPanel);
        vertical1.setResizeWeight(0.5);
        horizontal1.setResizeWeight(0.75);
        horizontal1.setDividerLocation(300);
        horizontal2.setResizeWeight(0.25);
        horizontal2.setDividerLocation(150);
        horizontal1.setBorder(new EmptyBorder(0, 0, 0, 0));
        horizontal2.setBorder(new EmptyBorder(0, 0, 0, 0));
        vertical1.setBorder(new EmptyBorder(0, 0, 0, 0));
        centerPanel.setLayout(new GridLayout(1, 1));
        centerPanel.setPreferredSize(new Dimension(300, 300));
        leftPanel.setLayout(new GridLayout(1, 1));
        leftPanel.setPreferredSize(new Dimension(100, 100));
        rightPanel.setLayout(new GridLayout(1, 1));
        rightPanel.setPreferredSize(new Dimension(150, 150));
        bottomPanel.setLayout(new GridLayout(1, 1));
        bottomPanel.setPreferredSize(new Dimension(100, 100));
        rootPanel.setLayout(new GridLayout(1, 1));
        rootPanel.add(vertical1);

        // General layout
        centerPanel.setBorder(new EmptyBorder(0, 0, 0, 0));
        centerPanel.add(tabView);
        bottomPanel.add(terminalView);
        leftPanel.add(clientView);
        rightPanel.add(treeView);
        setContentPane(rootPanel);
        setMinimumSize(new Dimension(600, 500));
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setSize(750, 550);
        setTitle("Runout server");
        setVisible(true);
    }

    private void setupToolbar() {
        JMenuBar toolbar = new JMenuBar();
        JMenu menuGame = new JMenu("Game");
        JMenu menuWorld = new JMenu("World");
        JMenu menuClient = new JMenu("Client");
        itemSave.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S, ActionEvent.META_MASK));
        menuGame.add(itemSave);
        itemLoad.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_O, ActionEvent.META_MASK));
        menuGame.add(itemLoad);
        menuGame.addSeparator();
        itemStart.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_1, ActionEvent.META_MASK));
        menuGame.add(itemStart);
        itemStop.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_2, ActionEvent.META_MASK));
        menuGame.add(itemStop);
        itemFog.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_F, ActionEvent.META_MASK));
        menuWorld.add(itemFog);
        menuWorld.addSeparator();
        JMenuItem itemSpawn = new JMenuItem("Spawn entity");
        itemSpawn.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_4, ActionEvent.META_MASK));
        menuWorld.add(itemSpawn);
        menuWorld.addSeparator();
        JMenuItem itemFreeze = new JMenuItem("Freeze");
        itemFreeze.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_3, ActionEvent.META_MASK));
        menuWorld.add(itemFreeze);
        itemDisconnect.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_K, ActionEvent.META_MASK));
        menuClient.add(itemDisconnect);
        toolbar.add(menuGame);
        toolbar.add(menuWorld);
        toolbar.add(menuClient);
        setJMenuBar(toolbar);
    }

    private void setupActions() {
        itemDisconnect.addActionListener(this);
        itemSave.addActionListener(this);
        itemLoad.addActionListener(this);
        itemStart.addActionListener(this);
        itemStop.addActionListener(this);
        itemFog.addItemListener(this);
        tabView.optionFog.addItemListener(this);
        tabView.optionDisconnect.addActionListener(this);
    }

    public void update() {
        treeView.update();
        if (Game.getFog()) {
            tabView.optionFog.setSelected(true);
            itemFog.setSelected(true);
        } else {
            tabView.optionFog.setSelected(false);
            itemFog.setSelected(false);
        }
    }

    public void updateClients() {
        clients.clear();
        for (ServerClient client : net.Server.getClients()) {
            clients.addElement(client);
        }
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == itemDisconnect || e.getSource() == tabView.optionDisconnect) {
            clientView.getSelectedValue().disconnect();
        }
        if (e.getSource() == itemLoad) {
            System.setProperty("apple.awt.fileDialogForDirectories", "false");
            FileDialog chooser = new FileDialog(this);
            chooser.setMode(FileDialog.LOAD);
            chooser.setTitle("Select file or folder");
            chooser.setFilenameFilter(new FilenameFilter() {
                @Override
                public boolean accept(File dir, String name) {
                    return name.endsWith(".rlvl");
                }
            });
            chooser.setVisible(true);
            GameFile.load(chooser.getFiles());
            update();
        }
        if (e.getSource() == itemSave) {
            GameFile.save("save");
        }
        if (e.getSource() == itemStart) {
            Game.start();
        }
        if (e.getSource() == itemStop) {
            Game.stop();
        }
        update();
    }

    @Override
    public void itemStateChanged(ItemEvent e) {
        if (e.getSource() == itemFog || e.getSource() == tabView.optionFog) {
            if (e.getStateChange() == 1) {
                Game.setFog(true);
            }
            if (e.getStateChange() == 2) {
                Game.setFog(false);
            }
            update();
        }
    }

}