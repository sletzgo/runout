package gui;

import javax.swing.JScrollPane;
import javax.swing.JPanel;
import javax.swing.JScrollBar;
import javax.swing.JTextArea;
import javax.swing.border.EmptyBorder;

import java.awt.GridLayout;
import java.awt.Color;
import java.awt.Dimension;

/**
 * terminalView
 */
@SuppressWarnings("serial")
public class TerminalView extends JPanel {

    public JTextArea terminal = new JTextArea();
    private JScrollPane scrollView;

    public TerminalView() {
        terminal.setBackground(Color.black);
        terminal.setForeground(Color.white);
        terminal.setFocusable(false);
        terminal.setEditable(false);
        scrollView = new JScrollPane(terminal);
        scrollView.setBorder(new EmptyBorder(0, 0, 0, 0));
        setMinimumSize(new Dimension(150, 150));
        setBackground(Color.black);
        setLayout(new GridLayout(1, 1));
        add(scrollView);
    }

    public void print(Object message) {
        print("Server", message);
    }

    public void print(String agent, Object message) {
        terminal.setText(terminal.getText() + "\n " + agent + " /> " + message);
        JScrollBar vertical = scrollView.getVerticalScrollBar();
        vertical.setValue(vertical.getMaximum());
        scrollView.validate();
    }

}