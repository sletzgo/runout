package gui;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTree;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreePath;

import core.io.GameFile;

import javax.swing.border.EmptyBorder;

import java.awt.event.MouseListener;
import java.awt.event.MouseEvent;
import java.awt.Dimension;
import java.awt.GridLayout;

import game.Entity;
import game.Event;
import game.Game;
import game.Trigger;
import game.World;
import game.WorldObject;

/**
 * TreeView
 */
@SuppressWarnings("serial")
public class TreeView extends JPanel implements MouseListener {

    public JTree tree;
    private JScrollPane scrollView;

    public TreeView() {
        setMinimumSize(new Dimension(150, 150));
        setLayout(new GridLayout(1, 1));
        update();
    }

    private DefaultMutableTreeNode createNodes() {
        DefaultMutableTreeNode top;
        game.Dimension[] dimensions = Game.getDimensions();
        if (dimensions.length <= 0) {
            top = new DefaultMutableTreeNode("Load game...");
            return top;
        }
        top = new DefaultMutableTreeNode("Dimensions");

        for (game.Dimension dimension : dimensions) {
            DefaultMutableTreeNode dimensionBranch = new DefaultMutableTreeNode(dimension.getName());
            top.add(dimensionBranch);

            DefaultMutableTreeNode worldsBranch = new DefaultMutableTreeNode("Worlds");
            dimensionBranch.add(worldsBranch);

            DefaultMutableTreeNode eventsBranch = new DefaultMutableTreeNode("Events");
            dimensionBranch.add(eventsBranch);

            for (World world : dimension.getWorlds()) {
                DefaultMutableTreeNode worldBranch = new DefaultMutableTreeNode(world.getName());
                worldsBranch.add(worldBranch);

                DefaultMutableTreeNode entitiesBranch = new DefaultMutableTreeNode("Entities");
                worldBranch.add(entitiesBranch);
                for (Entity entity : world.getEntities()) {
                    entitiesBranch.add(new DefaultMutableTreeNode(entity.getName()));
                }

                DefaultMutableTreeNode objectsBranch = new DefaultMutableTreeNode("Objects");
                worldBranch.add(objectsBranch);
                for (WorldObject object : world.getObjects()) {
                    objectsBranch.add(new DefaultMutableTreeNode(object.getResourcePath().toString(false)));
                }

                DefaultMutableTreeNode triggersBranch = new DefaultMutableTreeNode("Triggers");
                worldBranch.add(triggersBranch);
                for (Trigger trigger : world.getTriggers()) {
                    triggersBranch.add(new DefaultMutableTreeNode(trigger.getName()));
                }

            }
            for (Event event : dimension.getEvents()) {
                eventsBranch.add(new DefaultMutableTreeNode(event.getName()));
            }
        }

        return top;

    }

    public void update() {
        if (scrollView != null) {
            remove(scrollView);
        }
        if (tree != null) {
            tree.removeMouseListener(this);
        }
        tree = new JTree(createNodes());
        tree.setCellRenderer(new TreeRenderer());
        tree.setBorder(new EmptyBorder(8, 8, 8, 8));
        scrollView = new JScrollPane(tree);
        scrollView.setBorder(new EmptyBorder(0, 0, 0, 0));
        tree.addMouseListener(this);
        add(scrollView);
        expandTree(tree);
        validate();
    }

    public void expandTree(JTree tree) {
        int row = 0;
        while (row < tree.getRowCount()) {
            tree.expandRow(row);
            row++;
        }
    }

    @Override
    public void mousePressed(MouseEvent e) {
        int selRow = tree.getRowForLocation(e.getX(), e.getY());
        TreePath selPath = tree.getPathForLocation(e.getX(), e.getY());
        if (selRow >= 0) {
            if (selPath.getPathComponent(0).toString().equals("Load game...")) {
                GameFile.load("sample");
                update();
            }
            if (selPath.getPathCount() > 1 && selPath.getPathComponent(selPath.getPathCount() - 2).toString().equals("Worlds")) {
                ServerGUI.instance.tabView.setSelectedComponent(ServerGUI.instance.tabView.minimap);
                ServerGUI.instance.tabView.minimap.load(selPath.getLastPathComponent().toString());
            }
        }
    }

    @Override
    public void mouseClicked(MouseEvent e) {

    }

    @Override
    public void mouseReleased(MouseEvent e) {

    }

    @Override
    public void mouseEntered(MouseEvent e) {

    }

    @Override
    public void mouseExited(MouseEvent e) {

    }

}