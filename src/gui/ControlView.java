package gui;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.border.EmptyBorder;
import javax.swing.Timer;

import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.KeyEvent;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import levelbuilder.MiniMap;;

/**
 * ControlView
 */
@SuppressWarnings("serial")
public class ControlView extends JTabbedPane implements ActionListener {

    public MiniMap minimap = new MiniMap(false, true, false);
    public JButton optionDisconnect = new JButton("Disconnect");
    public JCheckBox optionFog = new JCheckBox("Fog of war");

    public ControlView() {
        setMinimumSize(new Dimension(300, 300));
        setBorder(new EmptyBorder(0, 0, 0, 0));
        JPanel tabServer = new JPanel();
        JPanel tabClient = new JPanel();

        addTab("Server", new ImageIcon("resources/server.png"), tabServer, "Server control panel");
        addTab("Client", new ImageIcon("resources/client.png"), tabClient, "Client options");
        addTab("Minimap", new ImageIcon("resources/pinpoint.png"), minimap, "Map of the selected world");
        setMnemonicAt(0, KeyEvent.VK_1);
        setMnemonicAt(1, KeyEvent.VK_2);
        setMnemonicAt(2, KeyEvent.VK_3);
        tabServer.setLayout(new GridLayout(2, 1, 12, 12));
        tabServer.setBorder(new EmptyBorder(25, 25, 25, 25));
        tabServer.add(optionDisconnect);
        tabServer.add(optionFog);

        Timer timer = new Timer(1000, this);
        timer.setInitialDelay(1);
        timer.start();
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        minimap.repaint();
    }
    
}