package gui;

import javax.swing.DefaultListModel;
import javax.swing.JList;
import javax.swing.border.EmptyBorder;

import net.ServerClient;

import static javax.swing.DefaultListSelectionModel.*;

import java.awt.Dimension;

/**
 * ClientView
 */
@SuppressWarnings("serial")
public class ClientView extends JList<ServerClient> {

	public ClientView(DefaultListModel<ServerClient> model) {
        super(model);
        setMinimumSize(new Dimension(150, 150));
        setSelectionMode(SINGLE_SELECTION);
        setBorder(new EmptyBorder(12, 12, 12, 12));
    }
    

    
}