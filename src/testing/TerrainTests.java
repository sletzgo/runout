package testing;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import game.Chunk;

class TerrainTests {

	@Test
	void getChunkCoordinateZero() {
		assertEquals(Chunk.getChunkCordAt(0), 0, "Unexpected calculation");
	}

	@Test
	void getChunkCoordinatePositive() {
		assertEquals(Chunk.getChunkCordAt(1), 0, "Unexpected calculation");
	}

	@Test
	void getChunkCoordinatePositiveOutside() {
		assertEquals(Chunk.getChunkCordAt(Chunk.SIZE), 1, "Unexpected calculation");
	}

	@Test
	void getChunkCoordinateNegative() {
		assertEquals(Chunk.getChunkCordAt(-1), -1, "Unexpected calculation");
	}

	@Test
	void getLocalCoordinateZero() {
		assertEquals(Chunk.getLocalCordAt(0), 0, "Unexpected calculation");
	}

	@Test
	void getLocalCoordinatePositive() {
		assertEquals(Chunk.getLocalCordAt(1), 1, "Unexpected calculation");
	}

	@Test
	void getLocalCoordinatePositiveOutside() {
		assertEquals(Chunk.getLocalCordAt(Chunk.SIZE), 0, "Unexpected calculation");
	}

	@Test
	void getLocalCoordinateNegative() {
		assertEquals(Chunk.getLocalCordAt(-1), Chunk.SIZE, "Unexpected calculation");
	}

	@Test
	void getLocalCoordinateNegativePlus() {
		if (Chunk.SIZE > 1)
			assertEquals(Chunk.getLocalCordAt(-2), Chunk.SIZE - 1, "Unexpected calculation");
	}

	@Test
	void getLocalCoordinateNegativeOuter() {
		assertEquals(Chunk.getLocalCordAt(-Chunk.SIZE), 0, "Unexpected calculation");
	}

}
