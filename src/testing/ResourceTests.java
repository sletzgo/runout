package testing;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import game.ResourcePath;

import game.Terrain;

class ResourceTests {

	@BeforeEach
    public void init() {
	}
	
	@AfterEach
	public void deinit() {
	}

	@Test
	void tileIDToResourcePathTest() {
		Terrain.loadResourcesIfNeeded();
		int ID = 0;
		ResourcePath resourcePath = Terrain.tileIDToResourcePath(ID);
		assertEquals(resourcePath, new ResourcePath("tile", "Water"), "Unexpected value");
	}

	@Test
	void resourcePathToTileIDTest() {
		Terrain.loadResourcesIfNeeded();
		ResourcePath resourcePath = new ResourcePath("tile", "Water");
		int ID = Terrain.resourcePathToTileID(resourcePath);
		assertEquals(ID, 0, "Unexpected value");
	}

	@Test
	void terrainResources() {
	}

	@Test
	void objectResources() {
	}

	@Test
	void entityResources() {
	}

}
